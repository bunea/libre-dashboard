import { useAuthContext } from "../../providers/AuthProvider";
import useDaoStats from "../queries/dao/useDaoStats";
import useUserDaoPower from "../queries/dao/useUserDaoPower";
import useUserTokens from "../queries/tokens/useUserTokens";

const emptyData = {
  data: {
    my_voting_power: "--",
    my_voting_percentage: "--",
    total_libre: "--",
  },
  isLoading: false,
};

export const useVotingPowerData = () => {
  const { currentUser } = useAuthContext();
  const { data: userPower, isLoading: isLoadingPower } = useUserDaoPower(
    currentUser.actor
  );
  const { data: daoStats, isLoading: isLoadingStats } = useDaoStats();
  const { data: userTokens, isLoading: isLoadingTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  if (!currentUser || !currentUser.actor) return emptyData;

  const libreTokens = userTokens?.find((ut) => ut.symbol === "LIBRE");

  return {
    data: {
      my_voting_power: Number(userPower?.voting_power).toLocaleString("en-US", {
        maximumFractionDigits: 0,
      }),
      my_voting_percentage: `${Number(
        (Number(userPower?.voting_power) / Number(daoStats?.voting_power)) * 100
      ).toFixed(2)}%`,
      total_libre: Number(libreTokens?.unstaked).toLocaleString("en-US", {
        maximumFractionDigits: 4,
        minimumFractionDigits: 4,
      }),
    },
    isLoading: isLoadingPower || isLoadingStats || isLoadingTokens,
  };
};
