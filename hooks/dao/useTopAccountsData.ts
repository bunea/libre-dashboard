import { sortBy } from "lodash";
import { IUserDaoPower } from "../../models/Dao";
import useDaoAccounts from "../queries/dao/useDaoAccounts";
import useDaoStats from "../queries/dao/useDaoStats";

export const useTopAccountsData = () => {
  const { data = [], isLoading, isFetched } = useDaoAccounts();
  const { data: daoStats } = useDaoStats();

  const sorted = sortBy(data, (d: IUserDaoPower) => Number(d.voting_power))
    .reverse()
    .filter((d) => d.voting_power > 0);
  const result = sorted.map((d: IUserDaoPower, i) => {
    return {
      rank: i + 1,
      user: d.account,
      voting_power: Number(d.voting_power).toLocaleString("en-US"),
      total_percentage: `${(
        (Number(d.voting_power) / Number(daoStats?.voting_power)) *
        100
      ).toFixed(2)}%`,
    };
  });

  return {
    data: result,
    isLoading,
    isFetched,
  };
};
