interface IUseMintCalculations {
  lockUpDays: number;
  mintBonus: number | undefined;
}

export const useMintCalculation = ({
  lockUpDays,
  mintBonus,
}: IUseMintCalculations) => {
  if (!lockUpDays || !mintBonus)
    return {
      baseAPY: 0,
      finalAPY: 0,
    };

  // CONSTANTS //
  let alpha0 = 0.1; // minimum yield
  let beta0 = 2.0; // maximum yield
  // --------- //

  const baseAPY = alpha0 + Math.sqrt(lockUpDays / 365) * (beta0 - alpha0);
  const finalAPY = baseAPY * (1 + mintBonus);

  return { baseAPY, finalAPY };
};
