import { useMemo } from "react";
import { CHAIN_CONFIG } from "../utils/constants";
import useTableRows from "./queries/contract/useTableRows";

export interface IExchangeRates {
  PBTC: number;
  BTCL: number;
  LIBRE: number;
  PUSDT: number;
  USDL: number;
  BTCUSD: number;
  BTCLIB: number;
  BTCSAT: number;
}

enum PoolNames {
  POOL1 = "pool1",
  POOL2 = "pool2",
}
type Numerator = PoolNames.POOL1 | PoolNames.POOL2;

const calculatePoolPrice = ({
  data,
  pool1Symbol,
  pool2Symbol,
  numerator,
  precision,
}: {
  data: any;
  pool1Symbol: string;
  pool2Symbol: string;
  numerator: Numerator;
  precision: number;
}) => {
  if (!data || !data[0]) return 0;
  const row = data[0];
  const pool1Amount = Number(
    row.pool1.quantity.replace(CHAIN_CONFIG()[pool1Symbol].symbol, "")
  );
  const pool2Amount = Number(
    row.pool2.quantity.replace(CHAIN_CONFIG()[pool2Symbol].symbol, "")
  );
  if (numerator === PoolNames.POOL1)
    return Number((pool1Amount / pool2Amount).toFixed(precision));
  return Number((pool2Amount / pool1Amount).toFixed(precision));
};

const getLibrePriceSats = (data: any[]) => {
  return calculatePoolPrice({
    data,
    pool1Symbol: "btc",
    pool2Symbol: "libre",
    numerator: PoolNames.POOL1,
    precision: 16,
  });
};

const getBTCPriceUSD = (data: any[]) => {
  return calculatePoolPrice({
    data,
    pool1Symbol: "btc",
    pool2Symbol: "usdt",
    numerator: PoolNames.POOL2,
    precision: 2,
  });
};

const calculateLPTokenValue = ({
  poolStats,
  btcPriceUSD,
  symbol,
}: {
  poolStats: any;
  btcPriceUSD: number;
  symbol: string;
}) => {
  if (!poolStats || !poolStats[0]) return 0;
  const pool1Amount = Number(String(poolStats[0].pool1.quantity).split(" ")[0]);
  const pool2Amount = Number(String(poolStats[0].pool2.quantity).split(" ")[0]);
  const supply = Number(String(poolStats[0].supply).split(" ")[0]);
  if (symbol === "BTCLIB") return (pool1Amount * 2 * btcPriceUSD) / supply;
  return (pool2Amount * 2) / supply;
};

export const useExchangeRates = () => {
  const { data: BTCUSDStats, isLoading: isLoadingBTCUSD } = useTableRows(
    {
      code: "swap.libre",
      scope: "BTCUSD",
      table: "stat",
    },
    { notifyOnChangeProps: ["data"], refetchOnWindowFocus: true }
  );
  const { data: BTCLIBStats, isLoading: isLoadingBTCLIB } = useTableRows(
    {
      code: "swap.libre",
      scope: "BTCLIB",
      table: "stat",
    },
    { notifyOnChangeProps: ["data"], refetchOnWindowFocus: true }
  );

  const librePriceSats = useMemo(
    () => getLibrePriceSats(BTCLIBStats ?? []),
    [BTCLIBStats]
  );
  const btcPriceUSD = useMemo(
    () => getBTCPriceUSD(BTCUSDStats ?? []),
    [BTCUSDStats]
  );
  const BTCUSDValue = useMemo(
    () =>
      calculateLPTokenValue({
        poolStats: BTCUSDStats,
        btcPriceUSD,
        symbol: "BTCUSD",
      }),
    [BTCUSDStats, btcPriceUSD]
  );
  const BTCLIBValue = useMemo(
    () =>
      calculateLPTokenValue({
        poolStats: BTCLIBStats,
        btcPriceUSD,
        symbol: "BTCLIB",
      }),
    [BTCLIBStats, btcPriceUSD]
  );

  return {
    data: {
      PBTC: btcPriceUSD,
      BTCL: btcPriceUSD,
      LIBRE: librePriceSats * btcPriceUSD,
      PUSDT: 1,
      USDL: 1,
      BTCUSD: BTCUSDValue,
      BTCLIB: BTCLIBValue,
      BTCSAT: librePriceSats,
    },
    isLoading: isLoadingBTCUSD || isLoadingBTCLIB,
  };
};
