import { useMemo } from "react";
import { displayTokenPrecisions, IUserToken } from "../models/Tokens";
import { useAuthContext } from "../providers/AuthProvider";
import useUserFarming from "./queries/farming/useUserFarming";
import useUserLiquidity from "./queries/tokens/useUserLiquidity";
import useUserTokens from "./queries/tokens/useUserTokens";

export const useTokenTableData = () => {
  const { currentUser } = useAuthContext();

  const {
    data: userLiquidity,
    isLoading: isLoadingUserLiquidity,
    isFetched: isFetchedUserLiquidity,
  } = useUserLiquidity(currentUser.actor);
  const {
    data: userFarming,
    isLoading: isLoadingUserFarming,
    isFetched: isFetchedUserFarming,
  } = useUserFarming(currentUser.actor);
  const {
    data: userTokens,
    isLoading: isLoadingUserTokens,
    isFetched: isFetchedUserTokens,
  } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data", "isLoading"],
  });

  const data = useMemo(() => {
    let tokenData: IUserToken[] = [];
    if (!userLiquidity || !userFarming || !userTokens) return tokenData;
    tokenData = userTokens;
    Object.keys(userLiquidity).forEach((key) => {
      const farmingToken = userFarming.find((item) => item.symbol === key);
      tokenData = [
        ...tokenData,
        {
          name: key,
          symbol: key,
          total:
            userLiquidity[key] && farmingToken?.total_staked
              ? Number(userLiquidity[key]) + Number(farmingToken?.total_staked)
              : 0,
          staked: farmingToken?.total_staked
            ? Number(farmingToken?.total_staked)
            : 0,
          total_staked_payout: farmingToken?.total_staked
            ? Number(farmingToken?.total_staked)
            : 0,
          unstaked: userLiquidity[key] ? Number(userLiquidity[key]) : 0,
          apy: null,
          enabled: true,
          icon: "",
          precision: displayTokenPrecisions[key],
        },
      ];
    });
    return tokenData;
  }, [userLiquidity, userFarming, userTokens]);

  return {
    data,
    isLoading:
      isLoadingUserTokens || isLoadingUserFarming || isLoadingUserLiquidity,
    isFetched:
      isFetchedUserTokens || isFetchedUserFarming || isFetchedUserLiquidity,
  };
};
