import { IUserToken } from "../models/Tokens";
import { IExchangeRates, useExchangeRates } from "./useExchangeRates";

export const useUSDConversion = (tokens: IUserToken[] | undefined) => {
  const { data } = useExchangeRates();

  let values: number[] = [];
  if (!data || !tokens) return 0;

  tokens.forEach((t) => {
    const rate = data[t.symbol as keyof IExchangeRates];
    if (!rate) return;
    values = [...values, rate * Number(t.total)];
  });

  return values.reduce((a, b) => a + b, 0);
};
