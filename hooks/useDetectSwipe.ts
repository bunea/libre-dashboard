import { useEffect, useState } from "react";

export const useDetectSwipe = () => {
  const [swipeLeft, setSwipeLeft] = useState<boolean>(false);
  const [swipeRight, setSwipeRight] = useState<boolean>(false);

  let xDown: number | null = null;
  let yDown: number | null = null;

  const getTouches = (evt) => {
    return (
      evt.touches || // browser API
      evt.originalEvent.touches
    ); // jQuery
  };

  const handleTouchStart = (evt) => {
    const firstTouch = getTouches(evt)[0];
    xDown = firstTouch.clientX;
    yDown = firstTouch.clientY;
  };

  const handleTouchMove = (evt) => {
    if (!xDown || !yDown) {
      return;
    }

    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;

    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if (Math.abs(xDiff) > Math.abs(yDiff)) {
      /*most significant*/
      if (xDiff > 0) {
        /* right swipe */
        setSwipeRight(true);
        setSwipeLeft(false);
      } else {
        /* left swipe */
        setSwipeRight(false);
        setSwipeLeft(true);
      }
    }
    /* reset values */
    xDown = null;
    yDown = null;
  };

  useEffect(() => {
    window.addEventListener("touchstart", handleTouchStart, false);
    window.addEventListener("touchmove", handleTouchMove, false);
    return () => {
      window.removeEventListener("touchstart", handleTouchStart);
      window.removeEventListener("touchmove", handleTouchMove);
    };
  }, []);

  useEffect(() => {
    setSwipeLeft(false);
    setSwipeRight(false);
  }, [swipeLeft, swipeRight]);

  return {
    swipeRight,
    swipeLeft,
  };
};
