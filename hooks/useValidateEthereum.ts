import { ethers } from "ethers";

export const useValidateEthereum = (address: string) => {
  let addressString = address;
  let amount: string | undefined = "";
  let isValid = false;

  if (
    address.includes("/transfer?address=") &&
    address.startsWith("ethereum:")
  ) {
    addressString = address.split("/transfer?address=")[1];
  }

  if (address.includes("?amount=")) {
    amount = addressString.split("?amount=").pop();
    addressString = addressString.split("?amount=")[0];
  }

  isValid = ethers.isAddress(addressString);

  return {
    address: addressString,
    amount: amount,
    isValid,
  };
};
