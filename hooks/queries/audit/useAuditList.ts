import { useQuery, UseQueryResult } from "react-query";
import { IAuditList } from "../../../models/Audits";
import APIClient from "../../../services/APIClient";
import { RQ_AUDIT_LIST } from "./keys";

const getAuditList = async () => {
  const data: IAuditList = await APIClient.fetchAuditList();
  return data;
};

export default function useAuditList(): UseQueryResult<IAuditList, Error> {
  return useQuery<IAuditList, Error>([RQ_AUDIT_LIST], () => getAuditList());
}
