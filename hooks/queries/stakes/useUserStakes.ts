import { useQuery, UseQueryResult } from "react-query";

import { IStake } from "../../../models/Stakes";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_STAKES } from "./keys";

const getUserStakes = async (accountName: string) => {
  if (!accountName) return [];
  const data: IStake[] = await LibreClient.fetchUserStakes(accountName);
  return data;
};

export default function useUserStakes(
  accountName: string
): UseQueryResult<IStake[], Error> {
  return useQuery<IStake[], Error>(
    [RQ_USER_STAKES, accountName],
    () => getUserStakes(accountName),
    {
      enabled: !!accountName,
      refetchInterval: 2000,
    }
  );
}
