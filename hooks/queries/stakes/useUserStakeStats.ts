import { useQuery, UseQueryResult } from "react-query";
import { IStakeStats } from "../../../models/Stakes";
import APIClient from "../../../services/APIClient";
import { RQ_USER_STAKE_STATS } from "./keys";

const getUserStakeStats = async (accountName: string) => {
  if (!accountName) return {};
  const data = await APIClient.fetchUserStakeStats(accountName);
  return data;
};

export default function useUserStakeStats(
  accountName: string
): UseQueryResult<IStakeStats, Error> {
  return useQuery<IStakeStats, Error>([RQ_USER_STAKE_STATS, accountName], () =>
    getUserStakeStats(accountName)
  );
}
