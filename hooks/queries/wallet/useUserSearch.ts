import { useQuery, UseQueryResult } from "react-query";
import ChainClient from "../../../services/ChainClient";
import { RQ_SEARCH_USERS } from "./keys";

interface IUserSearch {
  accountName: string | null;
  notifyOnChangeProps: any[];
}

const getUserSearch = async (query: string | null) => {
  if (!query) return [];
  const data = await ChainClient.searchUsers(query);
  return data;
};

export default function useUserSearch({
  accountName,
  notifyOnChangeProps,
}: IUserSearch): UseQueryResult<any, Error> {
  return useQuery<any, Error>(
    [RQ_SEARCH_USERS, accountName],
    () => getUserSearch(accountName),
    {
      enabled: !!accountName,
      notifyOnChangeProps,
    }
  );
}
