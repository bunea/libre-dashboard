import { useMutation, useQueryClient } from "react-query";
import LibreClient, {
  IFundLibre,
  IMintLibre,
} from "../../../services/LibreClient";
import { RQ_FUNDING_STATS, RQ_USER_FUNDING_STATS } from "./keys";

export const useFundLibre = () => {
  const queryClient = useQueryClient();
  return useMutation(
    ({ quantity, symbol }: IFundLibre) =>
      LibreClient.fundLibre({ quantity, symbol }),
    {
      onSuccess: async (response) => {
        setTimeout(() => {
          queryClient.invalidateQueries(RQ_FUNDING_STATS);
          queryClient.invalidateQueries(RQ_USER_FUNDING_STATS);
        }, 3000);
        return response;
      },
    }
  );
};
