import { useQuery } from 'react-query';
import APIClient from "../../../services/APIClient";
import { RQ_USER_TOTAL_CLAIMED } from "./keys";
import { useQueryClient } from 'react-query';

export const useUserTotalClaimed = (accountName: string) => {

    const queryClient = useQueryClient();

    return useQuery(
        ['userTotalClaimed', accountName],
        () => APIClient.fetchUserTotalClaimed(accountName),
        {
            onSuccess: async (response) => {
                setTimeout(() => {
                    queryClient.invalidateQueries(RQ_USER_TOTAL_CLAIMED);
                }, 3000);
                return response;
            },
        }
    );
};