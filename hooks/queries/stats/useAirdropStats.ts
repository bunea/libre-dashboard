import { useQuery, UseQueryResult } from "react-query";
import { IAirdropStats } from "../../../models/Stats";
import APIClient from "../../../services/APIClient";
import { RQ_AIRDROP_STATS } from "./keys";

const getAirdropStats = async () => {
  const data: IAirdropStats = await APIClient.fetchAirdropStats();
  return data;
};

export default function useAirdropStats(): UseQueryResult<
  IAirdropStats,
  Error
> {
  return useQuery<IAirdropStats, Error>([RQ_AIRDROP_STATS], getAirdropStats);
}
