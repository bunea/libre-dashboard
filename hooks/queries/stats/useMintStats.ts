import { useQuery, UseQueryResult } from "react-query";
import { IMintStats } from "../../../models/Stats";
import APIClient from "../../../services/APIClient";
import { RQ_MINT_STATS } from "./keys";

const getMintStats = async () => {
  const data: IMintStats = await APIClient.fetchMintStats();
  return data;
};

export default function useMintStats(): UseQueryResult<IMintStats, Error> {
  return useQuery<IMintStats, Error>([RQ_MINT_STATS], getMintStats);
}
