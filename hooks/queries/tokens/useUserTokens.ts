import { useQuery, UseQueryResult } from "react-query";

import LibreClient from "../../../services/LibreClient";
import { CHAIN_CONFIG } from "../../../utils/constants";
import { RQ_USER_TOKENS } from "./keys";

interface IUseUserTokens {
  accountName: string;
  notifyOnChangeProps: any[];
}

const addStakedPayouts = (
  data: Array<object>,
  stakesResponse: Array<object>
) => {
  if (data.length > 0 && stakesResponse && stakesResponse.length) {
    const totalStakedPayout = stakesResponse
      .filter((c) => c.status === 1)
      .reduce((a, b) => {
        if (b.payout && b.payout.split(" "))
          return Number(b.payout.split(" ")[0]) + a;
        return;
      }, 0);
    const LIBREIndex = data.findIndex((d) => d.symbol === "LIBRE");
    if (totalStakedPayout && LIBREIndex) {
      data = [
        ...data.slice(0, LIBREIndex),
        {
          ...data[LIBREIndex],
          total_staked_payout: totalStakedPayout ?? 0,
          total: (totalStakedPayout ?? 0) + Number(data[LIBREIndex].unstaked),
        },
        ...data.slice(LIBREIndex + 1),
      ];
    }
  }
  return data;
};

const filterContractTokens = (tokens: Array<object>) => {
  if (!tokens) return [];
  return tokens?.filter(
    (c) =>
      c.contract === "eosio.token" ||
      c.contract === "btc.ptokens" ||
      c.contract === "usdt.ptokens"
  );
};

const combineTokenStakes = (tokens: Array<object>, stakes: Array<object>) => {
  return tokens.map((token) => {
    const isLIBRE = token.symbol === "LIBRE";

    const tokenName = () => {
      const symbol = String(token.symbol);
      if (symbol === "BTCL" || symbol === "PBTC") return "btc";
      if (symbol === "USDL" || symbol === "PUSDT") return "usdt";
      if (symbol === "LIBRE") return "libre";
      return "libre";
    };

    const activeStakes = stakes
      .filter((c) => c.status === 1)
      .map((c) => Number(c.libre_staked.replace("LIBRE", "")))
      .reduce((a, b) => a + b, 0);

    return {
      name: token.symbol,
      symbol: token.symbol,
      total: isLIBRE ? token.amount + activeStakes : token.amount,
      staked: isLIBRE ? activeStakes : 0,
      unstaked: isLIBRE ? token.amount : token.amount,
      apy: isLIBRE ? "10-200%" : null,
      enabled: true,
      icon: CHAIN_CONFIG()[tokenName()].icon,
      precision: CHAIN_CONFIG()[tokenName()].precision,
    };
  });
};

const fillTokenList = (tokens: Array<object>) => {
  const tokenList = [
    CHAIN_CONFIG().btc.symbol,
    CHAIN_CONFIG().usdt.symbol,
    CHAIN_CONFIG().libre.symbol,
  ];

  tokenList.forEach((item) => {
    const resultHasToken = tokens.some((c) => c.symbol === item);
    let tokenName = "libre";
    if (item === "PBTC" || item === "BTCL") tokenName = "btc";
    if (item === "PUSDT" || item === "USDL") tokenName = "usdt";
    if (!resultHasToken) {
      tokens.push({
        name: item,
        symbol: item,
        total: 0,
        staked: 0,
        unstaked: 0,
        apy: null,
        enabled: true,
        icon: CHAIN_CONFIG()[tokenName] ? CHAIN_CONFIG()[tokenName].icon : "",
        precision: CHAIN_CONFIG()[tokenName]
          ? CHAIN_CONFIG()[tokenName].precision
          : 0,
      });
    }
  });

  const sorted = [];
  for (const item of tokenList) {
    const token = tokens.filter((c) => c.symbol === item)[0];
    sorted.push(token);
  }
  return sorted;
};

const getUserTokens = async (accountName: string) => {
  if (!accountName) return [];

  try {
    const tokensByAccount = await LibreClient.fetchUserTokens(accountName);
    const staked = await LibreClient.getStakeByAccount(accountName);

    const filterTokens = filterContractTokens(tokensByAccount.tokens);
    const tokensAndStakes = combineTokenStakes(filterTokens, staked);
    const userTokenList = fillTokenList(tokensAndStakes);

    const tokensWithTotalStaked = userTokenList.map((d) => {
      return {
        ...d,
        total_staked_payout: 0,
      };
    });

    const data = addStakedPayouts(tokensWithTotalStaked, staked);
    return data.map((dataItem) => ({ ...dataItem, addAsset: dataItem.symbol }));
  } catch (e) {
    return [];
  }
};

export default function useUserTokens({
  accountName,
  notifyOnChangeProps,
}: IUseUserTokens): UseQueryResult<any, Error> {
  return useQuery<any, Error>(
    [RQ_USER_TOKENS, accountName],
    () => getUserTokens(accountName),
    {
      refetchInterval: 10000, // REFETCH EVERY 1 SECOND IN ORDER TO CONSTANTLY UPDATE BALANCE
      enabled: !!accountName,
      notifyOnChangeProps,
    }
  );
}
