import { useQuery, UseQueryResult } from "react-query";
import { IUserMintRush } from "../../../models/Tokens";
import LibreClient from "../../../services/LibreClient";
import { RQ_USER_MINT_RUSH } from "./keys";

const getUserMintRush = async (accountName: string) => {
  if (!accountName) return undefined;
  const data: any = await LibreClient.getUserMintRush(accountName);
  if (!data || !data.length) return undefined;
  return data
    .map((d) => d[1])
    .find((d) => String(d.owner) === String(accountName));
};

export default function useUserMintRush(
  accountName: string
): UseQueryResult<IUserMintRush, Error> {
  return useQuery<IUserMintRush, Error>(
    [RQ_USER_MINT_RUSH, accountName],
    () => getUserMintRush(accountName),
    {
      enabled: !!accountName,
      refetchInterval: 2000,
    }
  );
}
