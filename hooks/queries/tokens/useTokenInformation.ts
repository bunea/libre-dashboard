import { useQuery, UseQueryResult } from "react-query";
import { ITokenInformation } from "../../../models/Tokens";
import LibreClient from "../../../services/LibreClient";
import { RQ_TOKEN_INFORMATION } from "./keys";

const getTokenInformation = async (): Promise<ITokenInformation[]> => {
  const data: ITokenInformation[] = await LibreClient.getTokenInformation();
  return data;
};

export default function useTokenInformation(): UseQueryResult<
  ITokenInformation[],
  Error
> {
  return useQuery<ITokenInformation[], Error>(
    [RQ_TOKEN_INFORMATION],
    getTokenInformation
  );
}
