import { useMutation } from "react-query";
import ChainClient from "../../../services/ChainClient";

export const useGetLightningFee = () => {
  return useMutation(
    ({ invoice, accountName }: { invoice: string; accountName: string }) =>
      ChainClient.fetchLightningFee({
        invoice,
        accountName,
      })
  );
};
