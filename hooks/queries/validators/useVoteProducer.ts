import { useMutation, useQueryClient } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_VOTES } from "./keys";

export const useVoteProducer = () => {
  const queryClient = useQueryClient();
  return useMutation((producer: string) => LibreClient.voteProducer(producer), {
    onSuccess: async () => {
      return queryClient.invalidateQueries([RQ_VOTES]);
    },
  });
};
