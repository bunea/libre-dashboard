import { useQuery, UseQueryResult } from "react-query";
import { IValidators } from "../../../models/Validators";
import APIClient from "../../../services/APIClient";
import { RQ_VALIDATORS } from "./keys";

const getValidators = async () => {
  const data: IValidators[] = await APIClient.fetchValidators();
  return data;
};

export default function useValidators(): UseQueryResult<IValidators[], Error> {
  return useQuery<IValidators[], Error>([RQ_VALIDATORS], getValidators);
}
