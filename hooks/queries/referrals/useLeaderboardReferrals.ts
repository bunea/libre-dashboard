import { useQuery, UseQueryResult } from "react-query";
import { ILeaderboardReferral } from "../../../models/Referrals";
import APIClient from "../../../services/APIClient";
import { RQ_LEADERBOARD_REFERRALS } from "./keys";

const getLeaderboardReferrals = async () => {
  const data: ILeaderboardReferral[] =
    await APIClient.fetchReferralLeaderboard();
  return data;
};

export default function useLeaderboardReferrals(): UseQueryResult<
  ILeaderboardReferral[],
  Error
> {
  return useQuery<ILeaderboardReferral[], Error>(
    [RQ_LEADERBOARD_REFERRALS],
    getLeaderboardReferrals
  );
}
