import { useQuery, UseQueryResult } from "react-query";
import LibreClient from "../../../services/LibreClient";
import { RQ_DAO_ACCOUNTS } from "./keys";

const getDaoAccounts = async () => {
  const daoPower = await LibreClient.getDaoPower();
  return daoPower;
};

export default function useDaoAccounts(): UseQueryResult<any, Error> {
  return useQuery<any, Error>([RQ_DAO_ACCOUNTS], getDaoAccounts);
}
