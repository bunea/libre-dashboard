import { useMemo } from "react";
import { IToken, IUserToken } from "../models/Tokens";
import { useAuthContext } from "../providers/AuthProvider";
import useTokens from "./queries/tokens/useTokens";
import useUserTokens from "./queries/tokens/useUserTokens";

// HOOK DESIGNED TO ELEGANTLY SWITCH BETWEEN BASE TOKEN DATA
// AND USER TOKENS DEPENDING ON LOGIN IN ORDER TO USE
// SWAP/LIQUIDITY WITHOUT LOGIN
export const useSwapTokenData = (): {
  tokens: IUserToken[];
  isLoadingTokens: boolean;
} => {
  const { currentUser } = useAuthContext();

  const { data: userTokens, isLoading: isLoadingUserTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data", "isLoading"],
  });
  const { data: tokens, isLoading: isLoadingTokens } = useTokens();

  const { data, isLoading } = useMemo(() => {
    if (!tokens) {
      return {
        data: [],
        isLoading: isLoadingTokens,
      };
    }

    const baseTokens = tokens.map((t: IToken): IUserToken => {
      return {
        name: t.name,
        symbol: t.symbol,
        apy: t.apy,
        enabled: true,
        staked: 0,
        total: t.total,
        unstaked: t.unstaked ?? 0,
        precision: t.precision,
        icon: t.icon,
        total_staked_payout: 0,
      };
    });

    if (!currentUser.actor) {
      return {
        data: baseTokens,
        isLoading: isLoadingTokens,
      };
    }

    const hydratedUserTokens = baseTokens.map((t: IUserToken) => {
      if (!userTokens) return t;
      const matchingToken = userTokens.find(
        (ut: IUserToken) => ut.symbol === t.symbol
      );
      if (!matchingToken) return t;
      return {
        ...t,
        ...matchingToken,
      };
    });

    return {
      data: hydratedUserTokens,
      isLoading: isLoadingUserTokens,
    };
  }, [currentUser.actor, userTokens, tokens, isLoadingTokens]);

  return {
    tokens: data,
    isLoadingTokens: isLoading,
  };
};
