import {
  AutoSizer,
  Column,
  Table,
  TableCellProps,
  TableHeaderProps,
} from "react-virtualized";
import styled from "styled-components";
import useAuditList from "../../hooks/queries/audit/useAuditList";
import {
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../uikit/Table";
import { satsToBitcoin } from "../../utils";

const HeadItem = styled.span`
  display: flex;
  align-items: center;
  cursor: pointer;
  font-size: 12.8px;
  font-weight: 600;
  color: ${(p) => p.theme.tableHeader};
  position: relative;
  white-space: nowrap;
  text-transform: none;
`;

const CellItem = styled.span`
  font-size: 15px;
  color: black;
`;

const OnChainValidationTable = () => {
  const { data: auditList, isLoading } = useAuditList();
  const data = auditList?.addresses ?? [];
  const { tableHeight, resolveRowStyle, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 5,
  });

  const headerRenderer = ({ label, dataKey }: TableHeaderProps) => {
    if (dataKey === "balance") {
      return (
        <HeadItem style={{ justifyContent: "flex-end" }}>{label}</HeadItem>
      );
    }
    return <HeadItem>{label}</HeadItem>;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "balance") {
      return <CellItem>{satsToBitcoin(cellData)}</CellItem>;
    }
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight < 100 ? 100 : tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <Table
            width={width < 600 ? 600 : width}
            height={height < 100 ? 100 : height}
            headerHeight={HEADER_HEIGHT}
            rowHeight={ROW_HEIGHT}
            rowCount={data.length}
            rowGetter={({ index }) => data[index]}
            rowStyle={({ index }) => resolveRowStyle(index + 1 === data.length)}
            overscanRowCount={5}
            noRowsRenderer={noRowsRenderer}
          >
            <Column
              label="PNetwork BTC Address"
              dataKey={"address"}
              flexGrow={1}
              flexShrink={1}
              width={100}
              minWidth={50}
              headerRenderer={headerRenderer}
              cellRenderer={cellRenderer}
            />
            <Column
              label="Balance Amount"
              dataKey={"balance"}
              flexGrow={0}
              flexShrink={0}
              width={150}
              minWidth={100}
              headerRenderer={headerRenderer}
              cellRenderer={cellRenderer}
              style={{ textAlign: "right" }}
            />
          </Table>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};

export default OnChainValidationTable;
