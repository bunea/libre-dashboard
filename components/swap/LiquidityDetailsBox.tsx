import { useMemo } from "react";
import styled from "styled-components";
import useUserLiquidity from "../../hooks/queries/tokens/useUserLiquidity";
import { usePoolSupply } from "../../hooks/tokens/usePoolSupply";
import { useAuthContext } from "../../providers/AuthProvider";
import { LiquidityDetailsBoxItem } from "./LiquidityDetailsBoxItem";

const Wrapper = styled.div`
  background: ${(p) => p.theme.detailBlockBackground};
  max-width: 552px;
  width: 100%;
  margin: 24px 0 0;
  padding: 20px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
`;

const Title = styled.span`
  display: inline-block;
  font-size: 19.2px;
  font-weight: 600;
  margin-bottom: 18px;
`;

const Table = styled.div`
  display: flex;
  flex-direction: column;
`;

export const LiquidityDetailsBox = () => {
  const { currentUser } = useAuthContext();
  const { data: userLiquidity } = useUserLiquidity(currentUser?.actor);
  const poolSupply = usePoolSupply();

  const poolDetails = useMemo(() => {
    if (!poolSupply || !userLiquidity) return [];
    return poolSupply.map((ps) => {
      return {
        poolName: `${ps.pool1.quantity.split(" ")[1]}/${
          ps.pool2.quantity.split(" ")[1]
        }`,
        pool1Quantity: Number(ps.pool1.quantity.split(" ")[0]),
        pool2Quantity: Number(ps.pool2.quantity.split(" ")[0]),
        pool1Symbol: ps.pool1.quantity.split(" ")[1],
        pool2Symbol: ps.pool2.quantity.split(" ")[1],
        supply: Number(ps.supply.split(" ")[0]),
        balance: Number(userLiquidity[ps.supply.split(" ")[1]]),
      };
    });
  }, [poolSupply, userLiquidity]);

  const hasBalance = useMemo(() => {
    const balances = poolDetails.filter((pd) => pd.balance > 0);
    if (!balances.length) return false;
    return true;
  }, [poolDetails]);

  const renderRows = () => {
    return poolDetails
      .filter((pd) => pd.balance > 0)
      .map((pd) => <LiquidityDetailsBoxItem data={pd} key={pd.poolName} />);
  };

  // TODO: ADD ZERO CHECK WHEN MULTIPLE POOLS ARE PRESENT
  if (!userLiquidity || !poolDetails || !poolDetails.length || !hasBalance)
    return <></>;

  return (
    <Wrapper>
      <Title>Your Liquidity</Title>
      <Table>{renderRows()}</Table>
    </Wrapper>
  );
};
