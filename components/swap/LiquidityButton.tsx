import { useContext } from "react";
import { useTheme } from "styled-components";
import { IUserToken } from "../../models/Tokens";
import { useAuthContext } from "../../providers/AuthProvider";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import { MediumButton } from "../../uikit/Button";

interface ILiquidityButton {
  activeTokenTo: IUserToken;
  toTokenValue: string;
  fromTokenValue: string;
  hasBalanceError: string | null;
  handleSubmit: () => void;
}

export const LiquidityButton = ({
  activeTokenTo,
  toTokenValue,
  fromTokenValue,
  hasBalanceError,
  handleSubmit,
}: ILiquidityButton) => {
  const theme = useTheme();
  const { currentUser, isLoadingUser } = useAuthContext();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);

  if (!currentUser || !currentUser.actor)
    return (
      <MediumButton
        disabled={isLoadingUser}
        text="Connect Wallet"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
        onClick={handleLogin}
      />
    );
  if (
    activeTokenTo.symbol === "LIBRE" &&
    Number(toTokenValue) < 300 &&
    Number(toTokenValue) > 0
  )
    return (
      <MediumButton
        disabled
        text={`300 LIBRE Minimum Required`}
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  if (
    (activeTokenTo.symbol === "USDT" || activeTokenTo.symbol === "USDL") &&
    Number(toTokenValue) < 1 &&
    Number(toTokenValue) > 0
  )
    return (
      <MediumButton
        disabled
        text={`1 USDT Minimum Required`}
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  if (!fromTokenValue || Number(fromTokenValue) == 0) {
    return (
      <MediumButton
        disabled
        text="Enter an amount"
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  }
  if (hasBalanceError)
    return (
      <MediumButton
        disabled
        text={hasBalanceError}
        color={theme.buttonGreyText}
        backgroundColor={"#f5f5f6"}
        border={theme.inputBorder}
        onClick={() => {}}
      />
    );
  return (
    <MediumButton
      text="Add Liquidity"
      color={theme.buttonOrangeText}
      backgroundColor={theme.buttonOrange}
      onClick={handleSubmit}
    />
  );
};
