import { useContext } from "react";
import Lottie from "react-lottie";
import styled, { css, useTheme } from "styled-components";
import { ConnectWalletContext } from "../../providers/ConnectWalletProvider";
import connectAnimation from "../../uikit/animations/connect-animation.json";

const Wrapper = styled.div<{ noBorder?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid ${(p) => p.theme.detailBlockBorder};
  border-radius: 16px;
  background: #ffffff;
  height: 450px;

  ${(p) =>
    p.noBorder &&
    css`
      border: none;
    `}
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ImageContainer = styled.div`
  margin-bottom: 24px;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  text-align: center;
  color: #04021d;
`;

const Description = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  text-align: center;
  color: ${(p) => p.theme.descriptionText};
  max-width: 294px;
  margin: 20px 0 30px;
`;

const BtnContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 144px;

  button {
    width: 100%;
  }
`;

export const EmptyContributions = ({ noBorder }: { noBorder?: boolean }) => {
  const theme = useTheme();
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: connectAnimation,
  };

  return (
    <Wrapper noBorder={noBorder}>
      <Content>
        <ImageContainer>
          <Lottie options={defaultOptions} width={180} height={180} />
        </ImageContainer>
        <Title>You have no contributions</Title>
        <Description>
          Looks like you have not contributed yet. Login and click "New Contribution" to get started.
        </Description>
        <BtnContainer>
          {/* <MediumButton
            text="Contribute Now"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
            onClick={handleLogin}
          /> */}
        </BtnContainer>
      </Content>
    </Wrapper>
  );
};
