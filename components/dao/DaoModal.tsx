import Image from "next/image";
import { useEffect, useState } from "react";
import { useQueryClient } from "react-query";
import styled, { useTheme } from "styled-components";
import { RQ_PROPOSALS } from "../../hooks/queries/proposals/keys";
import useProposalProperties from "../../hooks/queries/proposals/useProposalProperties";
import useUserTokens from "../../hooks/queries/tokens/useUserTokens";
import { useIsUsernameValid } from "../../hooks/useIsUsernameValid";
import { useAuthContext } from "../../providers/AuthProvider";
import LibreClient from "../../services/LibreClient";
import { MediumButton } from "../../uikit/Button";
import { EmptyInput } from "../../uikit/EmptyInput";
import { EmptyTextarea } from "../../uikit/EmptyTextarea";
import { Notification } from "../../uikit/Notification";
import AppModal from "../AppModal";
import { CharacterCount } from "../CharacterCount";
import { CurrencySelector } from "../swap/CurrencySelector";
import { CHAIN_CONFIG } from "../../utils/constants";
import { IUserToken } from "../../models/Tokens";

const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

const Text = styled.span`
  margin: 24px 8px 24px 0;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => p.theme.greyText};
`;

const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  justify-content: center;
  margin: 0 0 16px;

  > span {
    position: absolute !important;
    right: 15px !important;
  }
`;

const ProposalCurrencyContainer = styled.div`
  position: absolute;
  right: 4px;
`;

const NotificationContainer = styled.div`
  margin-bottom: 24px;
  width: 100%;
`;

function isValidHttpUrl(string: string) {
  let url;
  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }
  return url.protocol === "http:" || url.protocol === "https:";
}

const availableTokensForProposal: IUserToken[] = [
  {
    name: CHAIN_CONFIG().libre.symbol,
    symbol: CHAIN_CONFIG().libre.symbol,
    precision: CHAIN_CONFIG().libre.precision,
    icon: CHAIN_CONFIG().libre.icon,
    apy: 0,
    enabled: true,
    staked: 0,
    total: 0,
    unstaked: 0,
    total_staked_payout: 0,
  },
  {
    name: CHAIN_CONFIG().usdt.symbol,
    symbol: CHAIN_CONFIG().usdt.symbol,
    precision: CHAIN_CONFIG().usdt.precision,
    icon: CHAIN_CONFIG().usdt.icon,
    apy: 0,
    enabled: true,
    staked: 0,
    total: 0,
    unstaked: 0,
    total_staked_payout: 0,
  },
  {
    name: CHAIN_CONFIG().btc.symbol,
    symbol: CHAIN_CONFIG().btc.symbol,
    precision: CHAIN_CONFIG().btc.precision,
    icon: CHAIN_CONFIG().btc.icon,
    apy: 0,
    enabled: true,
    staked: 0,
    total: 0,
    unstaked: 0,
    total_staked_payout: 0,
  },
];

interface IDaoModal {
  open: boolean;
  handleOnClose: () => void;
}

export const DaoModal = ({ open, handleOnClose }: IDaoModal) => {
  const theme = useTheme();
  const queryClient = useQueryClient();
  const { currentUser } = useAuthContext();
  const { data: proposalProperties } = useProposalProperties();
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  const [inputValue, setInputValue] = useState<string>("");
  const [receiverValue, setReceiverValue] = useState<string>("");
  const [amountValue, setAmountValue] = useState<string>("");
  const [proposalToken, setProposalToken] = useState<IUserToken>(
    availableTokensForProposal.filter(
      (token) => token.symbol === CHAIN_CONFIG().libre.symbol
    )[0]
  );
  const [urlValue, setUrlValue] = useState<string>("");
  const [memoValue, setMemoValue] = useState<string>("");
  const [textareaValue, setTextareaValue] = useState<string>("");
  const [memoError, setMemoError] = useState<boolean>(false);
  const [titleError, setTitleError] = useState<boolean>(false);
  const [receiverError, setReceiverError] = useState<boolean>(false);
  const [detailsError, setDetailsError] = useState<boolean>(false);
  const [amountError, setAmountError] = useState<boolean>(false);
  const [balanceError, setBalanceError] = useState<boolean>(false);
  const [urlError, setUrlError] = useState<boolean>(false);

  const { isValid, handleChange: handleUsernameCheck } = useIsUsernameValid();

  const handleInputChange = (e) => {
    if (titleError) setTitleError(false);
    setInputValue(e.target.value);
    if (e.target.value.trim().length > 100) setTitleError(true);
  };
  const handleReceiverChange = (e) => {
    if (receiverError) setReceiverError(false);
    setReceiverValue(e.target.value);
    handleUsernameCheck(e.target.value);
  };

  const handleAmountChange = (e) => {
    const precision = proposalToken.precision;
    let value = e.target.value;
    if (amountError) setAmountError(false);
    if (value.split(".")[1] && value.split(".")[1].length > precision)
      value = String(Number(value).toFixed(precision));
    setAmountValue(value);
  };

  const handleTextareaChange = (e) => {
    if (detailsError) setDetailsError(false);
    setTextareaValue(e.target.value);
    if (e.target.value.trim().length > 1000) setDetailsError(true);
  };
  const handleUrlChange = (e) => {
    if (urlError) setUrlError(false);
    setUrlValue(e.target.value);
    if (e.target.value.trim().length > 100 || !isValidHttpUrl(urlValue))
      setUrlError(true);
  };

  const handleMemoChange = (e) => {
    if (memoError) setMemoError(false);
    setMemoValue(e.target.value);
    if (e.target.value.trim().length > 100) setMemoError(true);
  };

  const handleSetProposalToken = (token: IUserToken) => {
    setAmountValue("");
    setAmountError(false);
    setProposalToken(token);
  };

  const handleSubmit = async () => {
    if (!inputValue || !textareaValue || !amountValue || !receiverValue) {
      if (!inputValue) setTitleError(true);
      if (!textareaValue) setDetailsError(true);
      if (!amountValue) setAmountError(true);
      if (!receiverValue || !isValid) setReceiverError(true);
      return;
    }
    const { success } = await LibreClient.createProposal({
      title: inputValue,
      detail: textareaValue,
      amount: Number(amountValue).toFixed(proposalToken.precision),
      proposalToken: proposalToken.symbol,
      url: urlValue,
      receiver: receiverValue,
      cost: proposalProperties?.proposal_cost,
      memo: memoValue,
    });
    if (success) {
      setInputValue("");
      setAmountValue("");
      setTextareaValue("");
      queryClient.invalidateQueries(RQ_PROPOSALS);
    }
    handleOnClose();
  };

  useEffect(() => {
    if (!userTokens || balanceError) return;
    const libreTokens = userTokens.find((t) => t.symbol === "LIBRE");
    if (libreTokens) {
      if (
        Number(libreTokens.total) <
        Number(proposalProperties?.minimum_balance_to_create_proposals)
      ) {
        setBalanceError(true);
      }
    }
  }, [userTokens, proposalProperties, balanceError]);

  const renderButton = () => {
    if (balanceError) {
      return (
        <MediumButton
          disabled
          text={`Insufficient LIBRE balance`}
          color={theme.buttonGreyText}
          backgroundColor={"#f5f5f6"}
          border={theme.inputBorder}
          onClick={() => {}}
        />
      );
    }
    return (
      <MediumButton
        onClick={handleSubmit}
        text="Contribute now"
        color={theme.buttonOrangeText}
        backgroundColor={theme.buttonOrange}
      />
    );
  };

  return (
    <AppModal
      title="Create Proposal"
      show={open}
      onClose={handleOnClose}
      styles={{ marginTop: "-10%" }}
    >
      <Content>
        <Text>
          Enter in the details of your proposal. Once your proposal is
          submitted, there will be a voting period. If your proposal does not
          get 10% of the total LIBRE votes it will fail. If it does not get 50%
          of the "FOR" votes, it will also fail. Once it passes, it will need to
          be executed.
        </Text>
        <InputContainer>
          <EmptyInput
            onChange={handleInputChange}
            value={inputValue}
            placeholder="Title of proposal"
            error={titleError}
          />
          <CharacterCount value={inputValue} max={100} marginUp={9} />
        </InputContainer>
        <InputContainer>
          <EmptyInput
            onChange={handleReceiverChange}
            value={receiverValue}
            placeholder="Receiver"
            error={receiverError || (receiverValue.length && !isValid)}
          />
          {isValid && receiverValue.length ? (
            <Image src={"/icons/checkmark-circle.svg"} height={16} width={16} />
          ) : (
            <></>
          )}
        </InputContainer>
        <InputContainer>
          <EmptyInput
            onChange={handleAmountChange}
            type="number"
            value={amountValue}
            placeholder="Amount"
            error={amountError}
          />
          <ProposalCurrencyContainer>
            <CurrencySelector
              tokens={availableTokensForProposal}
              activeToken={proposalToken}
              onChange={handleSetProposalToken}
            />
          </ProposalCurrencyContainer>
        </InputContainer>
        <InputContainer>
          <EmptyInput
            onChange={handleMemoChange}
            value={memoValue}
            placeholder="Memo"
            error={memoError}
          />
          <CharacterCount value={memoValue} max={100} marginUp={9} />
        </InputContainer>
        <InputContainer>
          <EmptyTextarea
            onChange={handleTextareaChange}
            value={textareaValue}
            placeholder="Details of proposal"
            error={detailsError}
          />
          <CharacterCount value={textareaValue} max={1000} marginUp={17} />
        </InputContainer>
        <InputContainer>
          <EmptyInput
            onChange={handleUrlChange}
            value={urlValue}
            placeholder="Proposal Url"
            error={urlError}
          />
          <CharacterCount value={urlValue} max={100} marginUp={9} />
        </InputContainer>
        <NotificationContainer>
          <Notification
            type="warning"
            text={`Submitting a proposal does not guarantee payment. This proposal costs ${proposalProperties?.proposal_cost} to create which is non-refundable.`}
          />
        </NotificationContainer>
        {renderButton()}
      </Content>
    </AppModal>
  );
};
