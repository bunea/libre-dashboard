import {
  AutoSizer,
  Column,
  Index,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled from "styled-components";
import { useTopAccountsData } from "../../hooks/dao/useTopAccountsData";
import { SectionEmpty } from "../../uikit/SectionEmpty";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../uikit/Table";

const CellItem = styled.span`
  display: flex;
  align-items: center;
  font-size: 15px;
  color: black;
`;

export const TopAccountsTable = ({ shortList }: { shortList?: boolean }) => {
  const { data, isLoading, isFetched } = useTopAccountsData();

  const tableData = shortList ? data.slice(0, 10) : data;

  const isRowLoaded = ({ index }: Index) => !!tableData[index];
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data: tableData,
    isLoading,
    rows: 3,
  });

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    return <CellItem>{cellData ? cellData : "--"}</CellItem>;
  };

  if (isFetched && (!tableData || !tableData.length)) return <SectionEmpty />;

  return (
    <TableWrapper height={tableHeight} showBottomBorder>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                autoHeight={true}
                ref={registerChild}
                width={width < 700 ? 700 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={tableData.length}
                rowGetter={({ index }) => tableData[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === tableData.length)
                }
                onRowsRendered={onRowsRendered}
                overscanRowCount={5}
                noRowsRenderer={noRowsRenderer}
              >
                <Column
                  label="Rank"
                  dataKey="rank"
                  flexGrow={0}
                  flexShrink={1}
                  width={30}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label=""
                  dataKey="user"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Voting Power"
                  dataKey="voting_power"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Percentage of Total"
                  dataKey="total_percentage"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Proposal Voted"
                  dataKey="proposals_voted"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};
