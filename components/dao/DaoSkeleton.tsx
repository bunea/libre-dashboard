import styled from "styled-components";

export const Item = styled.div`
  --background-color: #e7e9ef;
  --width: 100%;

  animation: shimmer 1.25s infinite;
  background-color: var(--background-color);
  background-repeat: no-repeat;
  background-image: linear-gradient(
    to right,
    var(--background-color) 0%,
    white 50%,
    var(--background-color) 100%
  );
  border-radius: 0.5rem;
  height: 20px;
  opacity: 0.5;
  width: var(--width);

  @keyframes shimmer {
    0% {
      background-position: calc(var(--width) * -1) 0;
    }

    50% {
      opacity: 0.25;
    }

    100% {
      background-position: var(--width) 0;
    }
  }
`;

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  height: 105px;
  padding: 20px 0;
  justify-content: space-between;
  border-bottom: 1px solid ${(p) => p.theme.detailBlockBorder};
`;

const Col = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  width: 35%;
`;

const EndCol = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
  width: 25%;
`;

const Title = styled(Item)`
  height: 32px;
`;

const BtmRow = styled.div`
  display: flex;
  width: 80%;
`;

const Label = styled(Item)`
  height: 24px;
  width: 80px;
  margin-right: 10px;
`;

const Description = styled(Item)`
  height: 24px;
`;

const RightRow = styled.div`
  display: flex;
  align-items: center;
`;

const Bar = styled(Item)`
  height: 10px;
  width: 70%;
`;

const Number = styled(Item)`
  height: 20px;
  width: 100px;
  margin-left: 10px;
`;

export const DaoSkeleton = () => {
  return (
    <Wrapper>
      <Col>
        <Title />
        <BtmRow>
          <Label />
          <Description />
        </BtmRow>
      </Col>
      <EndCol>
        <RightRow>
          <Bar />
          <Number />
        </RightRow>
        <RightRow>
          <Bar />
          <Number />
        </RightRow>
      </EndCol>
    </Wrapper>
  );
};
