import styled, { useTheme } from "styled-components";
import { useVotingPowerData } from "../../hooks/dao/useVotingPowerData";
import { useAuthContext } from "../../providers/AuthProvider";
import { MediumButton } from "../../uikit/Button";
import { DetailsBlock } from "../../uikit/DetailsBlock";
import { PageTitle } from "../PageTitle";

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  > div {
    flex: 0 1 60%;
  }
`;

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 2%;
  margin-bottom: 25px;
`;

export const MyVotingPowerSection = ({
  handleModalOpen,
}: {
  handleModalOpen: () => void;
}) => {
  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { data, isLoading } = useVotingPowerData();

  return (
    <>
      <Header>
        <PageTitle
          title="My Voting Power"
          description="Your voting power is calculated based on the amount of staked LIBRE you have in stake.libre and is weighted by the remaining time in your stake. For more information, see the docs."
        />
        {currentUser.actor && (
          <MediumButton
            onClick={handleModalOpen}
            text="Create Proposal"
            color={theme.buttonOrangeText}
            backgroundColor={theme.buttonOrange}
          />
        )}
      </Header>
      <Row>
        <DetailsBlock
          title="My Voting Power"
          details={String(data.my_voting_power)}
          loading={isLoading}
        />
        <DetailsBlock
          title="My Percentage of Voting Power"
          details={String(data.my_voting_percentage)}
          loading={isLoading}
        />
        <DetailsBlock
          title="My Available LIBRE"
          details={String(data.total_libre)}
          loading={isLoading}
        />
      </Row>
    </>
  );
};
