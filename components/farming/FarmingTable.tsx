import styled from "styled-components";
import useFarmingStats from "../../hooks/queries/farming/useFarmingStats";
import useUserFarming from "../../hooks/queries/farming/useUserFarming";
import useUserLiquidity from "../../hooks/queries/tokens/useUserLiquidity";
import { emptyUserFarming } from "../../models/Farming";
import { useAuthContext } from "../../providers/AuthProvider";
import { HeadItem, useGetTableInfo } from "../../uikit/Table";
import { FarmingTableRow } from "./FarmingTableRow";

const LoadingWrap = styled.div`
  display: flex;
  align-items: center;
  height: 80px;
`;

const OverflowContainer = styled.div`
  overflow: auto;
`;

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr 1fr 1fr 1fr 0.25fr;
  width: 100%;
  min-width: 900px;
`;

const HeadCell = styled(HeadItem)`
  border-bottom: 1px solid ${(p) => p.theme.detailBlockBorder};
`;

const ActionHead = styled(HeadCell)`
  justify-content: end;
`;

const ErrorWarning = styled.span``;

const LoadingContainer = styled.div`
  grid-column-start: 1;
  grid-column-end: 7;
  margin-bottom: 20px;
`;

export const FarmingTable = () => {
  const { currentUser } = useAuthContext();
  const { data: farmingStats = [], isLoading, isError } = useFarmingStats();
  const { data: userFarming } = useUserFarming(currentUser?.actor);
  const { data: userLiquidity } = useUserLiquidity(currentUser?.actor);

  const { noRowsRenderer } = useGetTableInfo({
    data: farmingStats,
    isLoading,
    rows: 2,
  });

  const renderRows = () => {
    if (isError)
      return (
        <LoadingWrap>
          <ErrorWarning>Error loading data. Please refresh.</ErrorWarning>
        </LoadingWrap>
      );
    if (!farmingStats || !farmingStats.length)
      return <LoadingContainer>{noRowsRenderer()}</LoadingContainer>;
    return farmingStats.map((item, idx) => {
      let userData = userFarming?.find((d) => d.symbol === item.symbol);
      let balance = userLiquidity ? Number(userLiquidity[item.symbol]) : 0;
      return (
        <FarmingTableRow
          key={item.symbol}
          stats={item}
          userData={userData ?? emptyUserFarming}
          balance={balance}
          index={idx === 0 ? 0 : idx + 1}
        />
      );
    });
  };

  return (
    <OverflowContainer>
      <Wrapper>
        <HeadCell>Pairs</HeadCell>
        <HeadCell>APY</HeadCell>
        <HeadCell>Total Farm</HeadCell>
        <HeadCell>Your Farm</HeadCell>
        <HeadCell>Earned</HeadCell>
        <ActionHead>Action</ActionHead>
        {renderRows()}
      </Wrapper>
    </OverflowContainer>
  );
};
