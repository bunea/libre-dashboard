import {
  AutoSizer,
  Column,
  Index,
  InfiniteLoader,
  Table,
  TableCellProps,
} from "react-virtualized";
import styled, { css } from "styled-components";
import { usePoolsTableData } from "../../../hooks/usePoolsTableData";
import { IconPairs } from "../../../uikit/IconPairs";
import {
  headerRenderer,
  HEADER_HEIGHT,
  ROW_HEIGHT,
  TableWrapper,
  useGetTableInfo,
} from "../../../uikit/Table";

const CellItem = styled.span<{ orange?: boolean }>`
  display: flex;
  align-items: center;
  font-size: 15px;
  color: ${(p) => p.theme.titleText};

  ${(p) =>
    p.orange &&
    css`
      color: ${(p) => p.theme.labelOrange};
    `}
`;

const Break = styled.div`
  width: 10px;
`;

const Text = styled.div`
  background: black;
  width: 100%;
  height: 50px;
`;

export const PoolsTable = () => {
  const { data, isLoading } = usePoolsTableData();

  const isRowLoaded = ({ index }: Index) => !!data[index];
  const { resolveRowStyle, tableHeight, noRowsRenderer } = useGetTableInfo({
    data,
    isLoading,
    rows: 2,
  });

  const loadMoreRows = async () => {
    return;
  };

  const cellRenderer = ({ cellData, dataKey, rowData }: TableCellProps) => {
    if (dataKey === "pairs") {
      return (
        <CellItem>
          <IconPairs
            imgSrc1={"/icons/btc-asset-icon.svg"}
            imgSrc2={
              cellData.includes("LIBRE")
                ? "/icons/libre-asset-icon.svg"
                : "/icons/usdt-asset-icon.png"
            }
          />
          <Break />
          {cellData}
        </CellItem>
      );
    }
    if (dataKey === "apy") return <CellItem orange>{cellData}</CellItem>;
    return <CellItem>{cellData}</CellItem>;
  };

  return (
    <TableWrapper height={tableHeight}>
      <AutoSizer>
        {({ width, height }) => (
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={1}
          >
            {({ onRowsRendered, registerChild }) => (
              <Table
                autoHeight={true}
                ref={registerChild}
                width={width < 700 ? 700 : width}
                height={height}
                headerHeight={HEADER_HEIGHT}
                rowHeight={ROW_HEIGHT}
                rowCount={data.length}
                rowGetter={({ index }) => data[index]}
                rowStyle={({ index }) =>
                  resolveRowStyle(index + 1 === data.length)
                }
                onRowsRendered={onRowsRendered}
                overscanRowCount={5}
                noRowsRenderer={noRowsRenderer}
              >
                <Column
                  label="Pairs"
                  dataKey="pairs"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="APY"
                  dataKey="apy"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Total Liquidity Pool"
                  dataKey="total_liquidity_pool"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
                <Column
                  label="Farmed %"
                  dataKey="farmed_percentage"
                  flexGrow={1}
                  flexShrink={1}
                  width={100}
                  headerRenderer={headerRenderer}
                  cellRenderer={cellRenderer}
                />
              </Table>
            )}
          </InfiniteLoader>
        )}
      </AutoSizer>
    </TableWrapper>
  );
};
