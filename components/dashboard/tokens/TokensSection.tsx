import styled from "styled-components";
import { TokenTable } from "./TokenTable";

const Wrapper = styled.div`
  padding: 24px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 30px;
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;

export const TokensSection = () => {
  return (
    <>
      <Wrapper>
        <Header>
          <HeaderCol>
            <Title>Tokens</Title>
          </HeaderCol>
        </Header>
        <TokenTable />
      </Wrapper>
    </>
  );
};
