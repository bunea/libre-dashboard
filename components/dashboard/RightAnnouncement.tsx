import Link from "next/link";
import styled from "styled-components";
import { SmallButton } from "../../uikit/Button";
import { MediaQueryWidths } from "../../utils/constants";
import { ButtonContainer, Description, Title } from "./Announcements";

const Wrapper = styled.div`
  flex: 0 1 49%;
  border-radius: 16px;
  background-color: ${(p) => p.theme.rightAnnouncementBackground};
  background-image: url("/images/coins.svg");
  background-size: 100%;
  background-repeat: no-repeat;
  background-position: bottom 50% right -180px;
  padding: 25px;
  padding-bottom: 50px;
  position: relative;

  @media (max-width: ${MediaQueryWidths.small}px) {
    width: 100%;
    overflow: hidden;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 80%;
`;

export const RightAnnouncement = () => {
  return (
    <Wrapper>
      <Container>
        <Title>
          Mint Rush Ends
          <br />
          1/14/23
        </Title>
        <Description>
          Contribute to the Mint Rush and be a founder of the Libre chain.
        </Description>
        <ButtonContainer>
          <Link href="https://discord.com/invite/asJNsB6sYJ" passHref>
            <a target="_blank">
              <SmallButton
                text="Join Discord"
                onClick={() => {}}
                backgroundColor="rgba(255,255,255, 0.2)"
                color="#ffffff"
              />
            </a>
          </Link>
        </ButtonContainer>
      </Container>
    </Wrapper>
  );
};
