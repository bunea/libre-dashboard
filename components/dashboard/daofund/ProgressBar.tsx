import styled from "styled-components";
import { defaultTheme } from "../../../uikit/themeConfig";

const ProgressBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const MarksContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Mark = styled.div`
  height: 15px;
  background-color: ${defaultTheme.greyText};
  width: 2px;
  margin: auto;
  margin-bottom: 5px;
`;

const MarkNumber = styled.div`
  font-size: 12px;
  color: ${defaultTheme.greyText};
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
`;

const BarContainer = styled.div`
  flex: 1;
  height: 16px;
  background: ${defaultTheme.labelGreyBackground};
  border-radius: 12px;
`;

const Bar = styled.div<{ value: number }>`
  height: 100%;
  border-radius: inherit;
  background: ${defaultTheme.buttonOrange};
  width: ${({ value }) => `${value}%`};
  transition: width 0.5s ease; /* Add transition for smooth animation */
`;

const InfoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
`;

const InfoText = styled.div`
  font-size: 15px;
  color: ${defaultTheme.greyText};
`;

type ProgressBarProps = {
  day?: number;
  btcContributed?: string;
};

export const DaoFundProgressBar = ({ day, btcContributed }: ProgressBarProps) => {
  return (
    <ProgressBarWrapper>
      <MarksContainer>
        {[0, 10, 20, 30, 40, 50, 60].map((mark) => (
          <div key={mark}>
            <MarkNumber>{mark}</MarkNumber>
            <Mark />
          </div>
        ))}
      </MarksContainer>
      <Wrapper>
        <BarContainer>
          <Bar value={((day || 0) / 60) * 100} />
        </BarContainer>
      </Wrapper>
      <InfoContainer>
        <InfoText>Day {day || '-'}/60</InfoText>
        <InfoText>{btcContributed || '-'} BTC contributed today</InfoText>
      </InfoContainer>
    </ProgressBarWrapper>
  );
};
