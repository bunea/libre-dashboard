import { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { useDetectSwipe } from "../../hooks/useDetectSwipe";
import { MediaQueryWidths } from "../../utils/constants";
import { LeftAnnouncement } from "./LeftAnnouncement";
import { RightAnnouncement } from "./RightAnnouncement";

export const Title = styled.span`
  font-size: 39.8px;
  font-weight: 600;
  line-height: 1.21;
  color: #fff;
  font-weight: 300;

  span {
    background: ${(p) => p.theme.titleGradient};
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }
`;

export const Description = styled.span<{ color?: string }>`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: ${(p) => (p.color ? p.color : "#ffffff")};
  max-width: 80%;
  margin: 15px 0 20px;
`;

export const ButtonContainer = styled.div`
  width: 100%;
  position: absolute;
  bottom: 10px;
`;

const AnnouncementContainer = styled.div`
  display: flex;
  justify-content: space-between;

  @media (max-width: ${MediaQueryWidths.small}px) {
    display: none !important;
  }
`;

const MobileContainer = styled.div`
  display: none;
  flex-direction: column;

  @media (max-width: ${MediaQueryWidths.small}px) {
    display: flex;

    > div {
      width: 100%;
    }
  }
`;

const Controller = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const Control = styled.div<{ active?: boolean }>`
  width: 12px;
  height: 12px;
  background-color: #c0c0c7;
  border-radius: 50px;
  margin: 10px 5px;
  cursor: pointer;

  ${(p) =>
    p.active &&
    css`
      width: 9px;
      height: 9px;
      background: transparent;
      box-shadow: 0 0 0 2px #c0c0c7;
      border-radius: 50px;
    `}
`;

export const Announcements = () => {
  const [activeIndex, setActiveIndex] = useState<number>(0);

  const { swipeLeft, swipeRight } = useDetectSwipe();

  useEffect(() => {
    if (!swipeLeft && !swipeRight) return;
    if (swipeLeft) setActiveIndex(activeIndex === 0 ? 1 : 0);
    if (swipeRight) setActiveIndex(activeIndex === 0 ? 1 : 0);
  }, [swipeLeft, swipeRight]);

  return (
    <>
      <AnnouncementContainer>
        <LeftAnnouncement />
        <RightAnnouncement />
      </AnnouncementContainer>
      <MobileContainer>
        {activeIndex === 0 && <LeftAnnouncement />}
        {activeIndex === 1 && <RightAnnouncement />}
        <Controller>
          <Control
            onClick={() => setActiveIndex(0)}
            active={activeIndex === 0}
          />
          <Control
            onClick={() => setActiveIndex(1)}
            active={activeIndex === 1}
          />
        </Controller>
      </MobileContainer>
    </>
  );
};
