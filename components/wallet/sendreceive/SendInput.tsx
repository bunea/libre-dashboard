import Image from "next/image";
import { useContext, useMemo } from "react";
import styled from "styled-components";
import { useSendInput } from "../../../hooks/wallet/useSendInput";
import { WalletSendContext } from "../../../providers/WalletSendProvider";
import { EmptyInput } from "../../../uikit/EmptyInput";

const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
`;

const ImgContainer = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
  right: 10px;
`;

export const SendInput = () => {
  const { selectedToken, error: sendError } = useContext(WalletSendContext);
  const { value, error, onChange } = useSendInput();

  const handlePlaceholder = useMemo(() => {
    if (selectedToken.symbol === "LIBRE") return "Search for LIBRE Name";
    if (selectedToken.symbol === "PBTC" || selectedToken.symbol === "BTCL")
      return "Name, Lightning or BTC address";
    return "Name or ETH address";
  }, [selectedToken]);

  return (
    <>
      <Wrapper>
        <EmptyInput
          value={value}
          onChange={onChange}
          placeholder={handlePlaceholder}
          error={false}
        />
        <ImgContainer>
          <Image src={"/icons/magnifying-glass.svg"} height={18} width={18} />
        </ImgContainer>
      </Wrapper>
    </>
  );
};
