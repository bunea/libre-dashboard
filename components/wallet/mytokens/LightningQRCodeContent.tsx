import React, { useState } from "react";

import { LightinngAmountModal } from "./LightningAmountModal";
import { LightningQRCodeModal } from "./LightningQRCodeModal";

export const LightningQRCodeContent = () => {
  const [amount, setAmount] = useState("0");
  const [invoice, setInvoice] = useState("");

  return !invoice ? (
    <LightinngAmountModal
      amount={amount}
      setAmount={setAmount}
      setInvoice={setInvoice}
      invoice={invoice}
    />
  ) : (
    <LightningQRCodeModal invoice={invoice} amount={amount} setInvoice={setInvoice} />
  );
};
