import Image from "next/image";
import { QRCode } from "react-qrcode-logo";
import styled from "styled-components";
import { useBitcoinInvoice } from "../../../hooks/useBitcoinInvoice";

import BTCImage from "../../../public/icons/btc-asset-icon.svg";
import { LoadingAnimation } from "../../../uikit/LoadingAnimation";

const Container = styled.div`
  background-color: #fafafa;
  margin: -24px;
  border-top: 1px solid ${(p) => p.theme.labelGreenBackground};
  border-bottom: 1px solid ${(p) => p.theme.labelGreenBackground};
  height: 281px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const BottomContainer = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: space-between;
`;

const QRCodeContainer = styled.div`
  width: 190px;
  margin: auto;
  background-color: ${(p) => p.theme.buttonWhite};
  padding: 10px;
  border-radius: 16px;
  margin-top: 40px;
  margin-bottom: 40px;
  border: solid 1px #e6e6e6;
`;

const HeaderTitle = styled.div`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: ${(p) => p.theme.titleText};
`;

const Description = styled.div`
  color: ${(p) => p.theme.labelGrey};
  font-size: 14px;
  margin-top: 10px;
`;

export const BitcoinQRCodeContent = () => {
  const { bitcoinAddress, isLoading } = useBitcoinInvoice();

  const handleCopyToClipboard = () => {
    navigator.clipboard.writeText(bitcoinAddress);
  };

  return (
    <>
      <Container>
        {isLoading ? (
          <LoadingAnimation size="large" />
        ) : (
          <QRCodeContainer>
            <QRCode
              style={{}}
              value={bitcoinAddress}
              qrStyle="dots"
              eyeRadius={5}
              logoImage={BTCImage.src}
              logoHeight={50}
              logoWidth={50}
            />
          </QRCodeContainer>
        )}
      </Container>
      <BottomContainer>
        <div>
          <HeaderTitle>Bitcoin Address</HeaderTitle>
          <Description>{bitcoinAddress}</Description>
        </div>
        <Image
          src="/icons/copy-icon.svg"
          width={24}
          height={24}
          alt="copy icon"
          onClick={handleCopyToClipboard}
          style={{ cursor: "pointer" }}
        />
      </BottomContainer>
    </>
  );
};
