enum StakeStatus {
  STAKE_IN_PROGRESS = 1,
  STAKE_COMPLETED = 2,
  STAKE_CANCELED = 3,
}

export interface IStake {
  index: number;
  account: string;
  stake_date: string;
  stake_length: number;
  mint_bonus: string;
  libre_staked: string;
  apy: string;
  payout: string;
  payout_date: string;
  status: StakeStatus;
}

export interface IStakeStats {
  balance: number;
  earned: number;
  staked: number;
}
