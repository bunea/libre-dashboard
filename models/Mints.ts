export interface IMint {
  account: string;
  btc_contributed: string;
  contributionDate: string;
  day_of_mint_rush: number;
  index: number;
  libre_minted: string;
  mint_apy: string;
  mint_bonus: string;
  staked_days: number;
  status: number;
}

export interface IMintStats {
  contributed: number;
  minted: number;
  percentage: number;
}
