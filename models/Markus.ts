export interface IMarkusOffer {
  marketMaker: string;
  offerId: string;
  offerredAmount: { quantity: string; contract: string };
  orderId: string;
}
