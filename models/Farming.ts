export interface IUserFarming {
  account: string;
  claimed: string;
  last_claim: string;
  symbol: string;
  total_staked: string;
  unclaimed: string;
}

export interface IFarmingStats {
  symbol: string;
  total_staked: string;
  farming_staked: string;
  apy: number;
  reward_per_farm: number;
}

export const emptyUserFarming: IUserFarming = {
  account: "",
  claimed: "",
  last_claim: "",
  symbol: "",
  total_staked: "",
  unclaimed: "",
};
