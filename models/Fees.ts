export interface IPtokenFees {
  networkFee: number;
  operatorFee: number;
}

export interface IPNetworkFeesResponse {
  networkFee: number;
  networkFeeUsd: number;
  basisPoints: { nativeToHost: number; nativeToNative: number };
  minNodeOperatorFee: number;
  minNodeOperatorFeeUsd: number;
}

export interface IChain {
  chainId: string;
  isNative: boolean;
  fees: {
    networkFee: number;
    minNodeOperatorFee: number;
    basisPoints: {
      hostToNative?: number;
      nativeToNative?: number;
    };
  };
}

export interface IFeeData {
  networkFee: string;
  minimumProtocolFee: string;
  protocolFeeAmount: string | undefined;
  pegOutFee: string | undefined;
  pegInFee: string | undefined;
}
