export interface ISearchUser {
  code: string;
  count: number;
  payer: string;
  scope: string;
  table: string;
}

export interface ISendRecipient {
  destination: string;
  type: RecipientType;
}

export type RecipientType = "bitcoin" | "lightning" | "libre" | "ethereum";
