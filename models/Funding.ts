export interface IFundingStats {
  totalContributed: number;
  currentContributed: number;
  currentPeriod: number;
  endTime: number;
  libreValue: number;
}

interface Contribution {
  allocatedLibre: number;
  claimDate: number;
  contributedSats: number;
  contributionTime: number;
  id: number;
  stakeDate: number;
  stakeDays: number;
  status: number;
}

export interface IUserFundingStats {
  myContributions: number;
  initialAllocation: number;
  totalClaimed: number;
  contributions: Contribution[];
}
