export interface IApplicationStats {
  accounts: number;
  active: number;
  newAccounts: number;
  referrals: number;
}

export interface IStakeStats {
  totalStaked: number;
  percentage: number;
  apy: number;
}

export interface IAirdropStats {
  activated: number;
  claimed: number;
}

export interface IMintStats {
  btc_contributed: string;
  mint_bonus_today: number;
  mint_bonus_tomorrow: number;
  mint_rush_day: number;
  start_date: Date;
  end_date: Date;
}
