export interface ILightningWrappingAddress {
  lightning: string;
}

export interface IBitcoinWrappingAdresses {
  bitcoin: string;
}

export interface IWrappingAdresses {
  bitcoin: string;
  lightning: string;
}
