import moment from "moment";
import Head from "next/head";
import Image from "next/image";
import { useEffect, useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import { EmptyContributions } from "../components/mint/EmptyContributions";
import { MintModal } from "../components/mint/MintModal";

import { PageTitle } from "../components/PageTitle";
import { FundTable } from "../components/fund/FundTable";
import { AddBalanceModal } from "../components/wallet/mytokens/AddBalanceModal";
import useFundingStats from "../hooks/queries/funding/useFundingStats";
import { useUserTotalClaimed } from "../hooks/queries/funding/useTotalClaimed";
import useUserFundingStats from "../hooks/queries/funding/useUserFundingStats";
import { useAuthContext } from "../providers/AuthProvider";
import { DetailsBlock } from "../uikit/DetailsBlock";
import { formatCurrencyWithPrecision, formatThousands } from "../utils";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const TopContainer = styled.div`
  display: flex;
  justify-content: center;
  background: #eeeef2;
  margin: 0 -20px;
  padding: 0 20px 10px;
  width: 100vw;
  align-self: center;
`;

const TopContent = styled.div`
  max-width: 1200px;
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 2%;
  margin-bottom: 25px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: ${MediaQueryWidths.small}px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const HeaderCol = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  width: 100%;

  @media (max-width: ${MediaQueryWidths.small}px) {
    width: 100%;
  }
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
  margin-top: -26px;

  a {
    color: ${(p) => p.theme.textAccentOrange};
    text-decoration: underline;
    cursor: pointer;
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin: 0 0 15px 10px;
  }
`;

function renderTimeRemainingInPeriod(targetTimestamp?: number): string {
  if (!targetTimestamp) return "--";
  const targetTime = moment.unix(targetTimestamp);
  const currentTime = moment();

  const timeRemaining = moment.duration(targetTime.diff(currentTime));

  const hours = Math.floor(timeRemaining.asHours());
  const minutes = timeRemaining.minutes();
  const seconds = timeRemaining.seconds();

  const formattedTimeRemaining = `${(hours % 24)
    .toString()
    .padStart(2, "0")}:${minutes.toString().padStart(2, "0")}:${seconds
    .toString()
    .padStart(2, "0")}`;

  return formattedTimeRemaining;
}

export default function DaoFund() {
  const theme = useTheme();

  const { currentUser } = useAuthContext();
  const { data: fundingStats, isLoading: isLoadingFundingStats } =
    useFundingStats();
  const { data: userFundingStats, isLoading: isLoadingUserFundingStats } =
    useUserFundingStats(currentUser.actor);

  const [timeRemaining, setTimeRemaining] = useState(
    renderTimeRemainingInPeriod(fundingStats?.endTime)
  );

  const { data: userTotalClaimed, isLoading: isLoadingUserTotalClaimed } =
    useUserTotalClaimed(currentUser.actor);

  useEffect(() => {
    const intervalId = setInterval(() => {
      setTimeRemaining(renderTimeRemainingInPeriod(fundingStats?.endTime));
    }, 1000);

    // Clean up the interval when the component unmounts
    return () => {
      clearInterval(intervalId);
    };
  }, [fundingStats?.endTime]);

  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [balanceModalOpen, setBalanceModalOpen] = useState<boolean>(false);

  const fundingEnded = useMemo(() => {
    let now = moment().utc();
    if (now.isAfter(moment.utc(fundingStats?.endTime))) return true;
    return false;
  }, [fundingStats]);

  const renderMyMints = () => {
    return (
      <Header>
        <HeaderCol>
          <PageTitle title="My Contributions" />
          <HeaderDescription>
            You will know the amount of LIBRE minted at the end of the Funding
            Period
          </HeaderDescription>
        </HeaderCol>
      </Header>
    );
  };

  const renderMintDay = () => {
    if (fundingEnded) return "--";
    if (
      typeof fundingStats?.currentPeriod !== "number" ||
      fundingStats.currentPeriod == null
    )
      return "--";

    const currentTimestamp = new Date().getTime(); // Current timestamp in milliseconds

    // Calculate the time difference in milliseconds
    const timeDifference = fundingStats.endTime - currentTimestamp;

    // Convert milliseconds to days (1 day = 24 hours * 60 minutes * 60 seconds * 1000 milliseconds)
    const totalDays = Math.ceil(
      timeDifference / (24 * 60 * 60 * 1000) + fundingStats.currentPeriod
    );

    if (fundingStats.currentPeriod > totalDays - 1) return "Ended";

    return `${Number(fundingStats?.currentPeriod) + 1} of ${totalDays}`;
  };

  const renderValueOfLibre = () => {
    if (!fundingStats?.libreValue) return "--";
    return `${Number(fundingStats.libreValue).toLocaleString("en-US", {
      maximumFractionDigits: 2,
    })} SATS`;
  };

  return (
    <>
      <Head>
        <title>LIBRE | DAO Fund</title>
      </Head>
      <Wrapper>
        <TopContainer>
          <TopContent>
            <Header style={{ marginBottom: 25 }}>
              <HeaderCol>
                <PageTitle title="LibreLaunch DAO Fund" />
                <HeaderDescription>
                  Send contributions of pBTC, receive LIBRE staked for 6 months.
                  There will be 10M LIBRE available daily divided pro-rata
                  amongst all contributors for that day.
                </HeaderDescription>
              </HeaderCol>
            </Header>
            <Row>
              <DetailsBlock
                title="Total Contributed to DAO"
                details={`${formatCurrencyWithPrecision(
                  fundingStats?.totalContributed || 0,
                  0
                )} SATS`}
                totalBlocks={2}
                image={
                  <Image
                    src="/logos/bitcoin-circle.svg"
                    width={32}
                    height={32}
                  />
                }
                loading={isLoadingFundingStats}
              />
              <DetailsBlock
                title="Contributed this period"
                details={`${formatCurrencyWithPrecision(
                  fundingStats?.currentContributed || 0,
                  0
                )} SATS`}
                totalBlocks={2}
                image={
                  <Image
                    src="/logos/bitcoin-circle.svg"
                    width={32}
                    height={32}
                  />
                }
                loading={isLoadingFundingStats}
              />
            </Row>
            <Row>
              <DetailsBlock
                title="Fund Period"
                details={renderMintDay()}
                loading={isLoadingFundingStats}
              />
              <DetailsBlock
                title="Time remaining in period"
                details={timeRemaining}
                loading={isLoadingFundingStats}
              />
              <DetailsBlock
                title="Value of LIBRE in period"
                details={renderValueOfLibre()}
                loading={isLoadingFundingStats}
              />
            </Row>
          </TopContent>
        </TopContainer>
        <Row>{renderMyMints()}</Row>
        <Row>
          <DetailsBlock
            title="My Contributed"
            details={
              userFundingStats?.myContributions
                ? `${formatCurrencyWithPrecision(
                    userFundingStats.myContributions,
                    0
                  )} SATS`
                : "--"
            }
            loading={isLoadingUserFundingStats}
          />
          <DetailsBlock
            title="My Initial Allocation"
            details={
              userFundingStats?.initialAllocation
                ? formatCurrencyWithPrecision(
                    userFundingStats?.initialAllocation,
                    0
                  ) + " LIBRE"
                : "--"
            }
            loading={isLoadingUserFundingStats}
          />
          <DetailsBlock
            title="My Total Claimed LIBRE"
            details={
              currentUser && currentUser.actor && userTotalClaimed
                ? formatThousands(userTotalClaimed)
                : "--"
            }
            loading={isLoadingUserTotalClaimed}
          />
        </Row>
        {currentUser && currentUser.actor ? (
          <FundTable />
        ) : (
          <EmptyContributions />
        )}
        <MintModal open={modalOpen} handleOnClose={() => setModalOpen(false)} />
        <AddBalanceModal
          open={balanceModalOpen}
          handleOnClose={() => setBalanceModalOpen(false)}
        />
      </Wrapper>
    </>
  );
}
