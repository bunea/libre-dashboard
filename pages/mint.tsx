import moment from "moment";
import Head from "next/head";
import Image from "next/image";
import { useContext, useMemo, useState } from "react";
import styled, { useTheme } from "styled-components";
import { EmptyContributions } from "../components/mint/EmptyContributions";
import { MintModal } from "../components/mint/MintModal";
import MintTable from "../components/mint/MintTable";
import { MyAvailableBTC } from "../components/mint/MyAvailableBTC";
import { PageTitle } from "../components/PageTitle";
import { AddBalanceModal } from "../components/wallet/mytokens/AddBalanceModal";
import useUserMintStats from "../hooks/queries/mints/useUserMintStats";
import useMintStats from "../hooks/queries/stats/useMintStats";
import { useAuthContext } from "../providers/AuthProvider";
import { ConnectWalletContext } from "../providers/ConnectWalletProvider";
import { MediumButton } from "../uikit/Button";
import { DetailsBlock } from "../uikit/DetailsBlock";
import { MediaQueryWidths } from "../utils/constants";

const Wrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
  background: ${(p) => p.theme.backgroundColor};
`;

const TopContainer = styled.div`
  display: flex;
  justify-content: center;
  background: #eeeef2;
  margin: 0 -20px;
  padding: 0 20px 10px;
  width: 100vw;
  align-self: center;
`;

const TopContent = styled.div`
  max-width: 1200px;
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 2%;
  margin-bottom: 25px;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media (max-width: ${MediaQueryWidths.small}px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const HeaderCol = styled.div`
  display: flex;
  flex-direction: column;
  width: 60%;

  @media (max-width: ${MediaQueryWidths.small}px) {
    width: 100%;
  }
`;

const HeaderDescription = styled.span`
  font-size: 14px;
  font-weight: 500;
  line-height: 1.71;
  color: #9695a0;
  margin-top: -26px;

  a {
    color: ${(p) => p.theme.textAccentOrange};
    text-decoration: underline;
    cursor: pointer;
  }

  @media (max-width: ${MediaQueryWidths.small}px) {
    margin: 0 0 15px 10px;
  }
`;

export default function Mint() {
  const theme = useTheme();
  const { currentUser } = useAuthContext();
  const { data: mintStats, isLoading: isLoadingMintStats } = useMintStats();
  const { data: userMintStats, isLoading: isLoadingUserMintStats } =
    useUserMintStats(currentUser.actor);
  const { handleOnOpen: handleLogin } = useContext(ConnectWalletContext);

  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [balanceModalOpen, setBalanceModalOpen] = useState<boolean>(false);

  const isPassedMintStart = useMemo(() => {
    let now = moment().utc();
    if (now.isAfter(moment.utc("2022-12-15T00:00:00-00:00"))) return true;
    return false;
  }, []);

  const mintEnded = useMemo(() => {
    let now = moment().utc();
    if (now.isAfter(moment.utc(mintStats?.end_date))) return true;
    return false;
  }, []);

  const renderMyMints = () => {
    return (
      <Header>
        <HeaderCol>
          <PageTitle title="My Mints" />
          <HeaderDescription>
            You will know the amount of LIBRE minted at the end of the Mint
            Rush. Starting the day after the Mint Rush ends, your LIBRE will be
            staked for the duration you chose.{" "}
            {currentUser && currentUser.actor && (
              <a onClick={() => setBalanceModalOpen(true)}>
                Don't have SATS to participate?
              </a>
            )}
          </HeaderDescription>
        </HeaderCol>
        {!mintEnded && (
          <MediumButton
            backgroundColor={theme.buttonOrange}
            color={theme.buttonOrangeText}
            text="New Contribution"
            onClick={
              currentUser && currentUser.actor
                ? () => setModalOpen(true)
                : handleLogin
            }
          />
        )}
      </Header>
    );
  };

  const renderMintDay = () => {
    if (mintEnded) return "--";
    if (!mintStats?.mint_rush_day) return "--";

    const totalDays = moment(mintStats?.end_date).diff(
      moment(mintStats?.start_date),
      "days"
    );

    if (mintStats.mint_rush_day > totalDays) return "Ended";

    return `${String(mintStats?.mint_rush_day)} of ${totalDays}`;
  };

  const renderTodayMultiplier = () => {
    if (mintEnded) return "--";
    if (!mintStats?.mint_bonus_today) return "--";

    const totalDays = moment(mintStats?.end_date).diff(
      moment(mintStats?.start_date),
      "days"
    );

    if (mintStats.mint_rush_day > totalDays) return "--";
    return `${Number(
      Number(mintStats?.mint_bonus_today.toFixed(2)) + 1
    ).toFixed(2)}x`;
  };

  const renderTomorrowMultiplier = () => {
    if (mintEnded) return "--";
    if (!mintStats?.mint_bonus_tomorrow) return "--";

    const totalDays = moment(mintStats?.end_date).diff(
      moment(mintStats?.start_date),
      "days"
    );

    if (mintStats.mint_rush_day > totalDays) return "--";
    return `${Number(
      Number(mintStats?.mint_bonus_tomorrow.toFixed(2)) + 1
    ).toFixed(2)}x`;
  };

  return (
    <>
      <Head>
        <title>LIBRE | Mint Rush</title>
      </Head>
      <Wrapper>
        <TopContainer>
          <TopContent>
            <PageTitle
              title="Mint Stats"
              description={
                isPassedMintStart && !mintEnded ? (
                  <>
                    Mint rush is live until Jan 14, 2023. See{" "}
                    <a href="https://www.libre.org/mint-rush">
                      https://www.libre.org/mint-rush
                    </a>{" "}
                    for details.
                  </>
                ) : (
                  ""
                )
              }
            />
            <Row>
              <DetailsBlock
                title="Total Contributed"
                details={
                  mintStats?.btc_contributed
                    ? `${mintStats?.btc_contributed} PBTC`
                    : "--"
                }
                totalBlocks={2}
                image={
                  <Image
                    src="/logos/bitcoin-circle.svg"
                    width={32}
                    height={32}
                  />
                }
                loading={isLoadingMintStats}
              />
              <DetailsBlock
                title="Total To Be Minted"
                details={"200,000,000 LIBRE"}
                totalBlocks={2}
                image={
                  <Image
                    src="/icons/libre-asset-icon.svg"
                    width={32}
                    height={32}
                  />
                }
                loading={isLoadingMintStats}
              />
            </Row>
            <Row>
              <DetailsBlock
                title="Mint rush day"
                details={renderMintDay()}
                loading={isLoadingMintStats}
              />
              <DetailsBlock
                title="Multiplier today"
                details={renderTodayMultiplier()}
                loading={isLoadingMintStats}
              />
              <DetailsBlock
                title="Multiplier tomorrow"
                details={renderTomorrowMultiplier()}
                loading={isLoadingMintStats}
              />
            </Row>
          </TopContent>
        </TopContainer>
        <Row>{renderMyMints()}</Row>
        <Row>
          <DetailsBlock
            title="My Contributed"
            details={
              userMintStats?.contributed
                ? `${Number(userMintStats?.contributed).toFixed(9)} PBTC`
                : "--"
            }
            loading={isLoadingUserMintStats}
          />
          <DetailsBlock
            title="My Total Percentage"
            details={
              userMintStats?.percentage
                ? Number(userMintStats?.percentage).toFixed(2) + "%"
                : "--"
            }
            loading={isLoadingUserMintStats}
          />
          <MyAvailableBTC />
        </Row>
        {currentUser && currentUser.actor ? (
          <MintTable />
        ) : (
          <EmptyContributions />
        )}
        <MintModal open={modalOpen} handleOnClose={() => setModalOpen(false)} />
        <AddBalanceModal
          open={balanceModalOpen}
          handleOnClose={() => setBalanceModalOpen(false)}
        />
      </Wrapper>
    </>
  );
}
