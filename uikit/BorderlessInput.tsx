import { FocusEventHandler, useEffect, useState } from "react";
import styled, { css } from "styled-components";

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

const Input = styled.input<{
  error?: boolean;
  hasLeft?: boolean;
  hasRight?: boolean;
}>`
  border: none;
  background-color: #fff;
  font-size: 4vmax;
  line-height: 1.5;
  outline: none;
  text-align: center;
  min-width: 4ch;

  &::placeholder {
    /* Chrome, Firefox, Opera, Safari 10.1+ */
    font-size: 4vmax;
    line-height: 1.5;
    color: #777685;
    opacity: 1; /* Firefox */
  }

  &:-ms-input-placeholder {
    /* Internet Explorer 10-11 */
    font-size: 4vmax;
    line-height: 1.5;
    color: #777685;
  }

  &::-ms-input-placeholder {
    /* Microsoft Edge */
    font-size: 4vmax;
    line-height: 1.5;
    color: #777685;
  }

  ${(p) =>
    p.error &&
    css`
      border: 1px solid ${(p) => p.theme.inputErrorBorder};

      &:focus {
        outline: 1px solid ${(p) => p.theme.inputErrorBorder};
      }
    `}

  ${(p) =>
    p.hasLeft &&
    css`
      padding-left: 80px;
    `}
`;

interface IBorderlessInput {
  placeholder: string;
  type?: string;
  value: string | number;
  onChange: (payload: any) => void;
  onFocus: (e: FocusEventHandler<HTMLInputElement>) => void;
  error?: boolean;
}

export const BorderlessInput = ({
  placeholder,
  type = "text",
  value,
  onChange,
  onFocus,
  error,
}: IBorderlessInput) => {
  const [size, setSize] = useState<number>(1);

  useEffect(() => {
    setSize(String(value).length);
  }, [value]);

  return (
    <Wrapper>
      <Input
        type={type}
        placeholder={placeholder}
        onChange={onChange}
        onFocus={onFocus}
        value={value}
        error={error}
        style={{ width: `${size}ch` }}
      />
    </Wrapper>
  );
};
