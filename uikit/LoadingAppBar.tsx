import React from "react";
import styled, { keyframes } from "styled-components";

const increase = keyframes`
  from { left: -5%; width: 5%;}
  to { left: 130%; width: 100%;}
`;

const decrease = keyframes`
  from { left: -80%; width: 80%; }
  to { left: 110%; width: 10%;}
`;

const Slider = styled.div`
  position: absolute;
  width: 100%;
  height: 5px;
  overflow-x: hidden;
  left: 0;
  bottom: -5px;
`;

const Line = styled.div`
  position: absolute;
  opacity: 0.1;
  background: black;
  width: 150%;
  height: 5px;
`;

const Subline = styled.div`
  position: absolute;
  background: ${(p) => p.theme.headerTextHover};
  height: 5px;
`;

const SublineInc = styled(Subline)`
  animation: ${increase} 2s infinite;
`;

const SublineDec = styled(Subline)`
  animation: ${decrease} 2s 0.5s infinite;
`;

export const LoadingAppBar = () => {
  return (
    <Slider>
      <Line />
      <SublineInc />
      <SublineDec />
    </Slider>
  );
};
