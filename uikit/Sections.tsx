import styled from "styled-components";
import { MediaQueryWidths } from "../utils/constants";

export const SectionWrapper = styled.div`
  padding: 24px 24px 0;
  border-radius: 16px;
  border: solid 1px ${(p) => p.theme.detailBlockBorder};
  background-color: #fff;

  @media (max-width: ${MediaQueryWidths.small}px) {
    padding: 10px 10px 0;
  }
`;

export const SectionTitle = styled.span`
  font-size: 19.2px;
  font-weight: 600;
  line-height: 1.25;
  color: #110f28;
  display: inline-block;
`;
