import styled from "styled-components";
import { LoadingAnimation } from "./LoadingAnimation";

const Container = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  padding: 20px 0 40px;
`;

export const SectionLoading = () => {
  return (
    <Container>
      <LoadingAnimation size="medium" />
    </Container>
  );
};
