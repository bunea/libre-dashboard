import Lottie from "react-lottie";
import styled, { css } from "styled-components";
import loadButtonAnimation from "./animations/button-loading-animation.json";

interface IButton {
  text: any;
  backgroundColor?: string;
  border?: string;
  color?: string;
  onClick: () => void;
  buttonStyle?: "empty" | "emptyGradient" | "normal";
  disabled?: boolean;
  isLoading?: boolean;
}

const ButtonStyled = styled.button<{
  backgroundColor: string;
  color: string;
  border?: string;
  buttonStyle?: "empty" | "emptyGradient" | "normal";
}>`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 20px;
  background: ${(p) => p.backgroundColor};
  color: ${(p) => p.color};
  border: ${(p) => (p.border ? `1.5px solid ${p.border}` : "none")};
  padding: ${(p) =>
    p.buttonStyle === "normal"
      ? "7.5px 11.5px 7.5px 11.5px"
      : "6px 10px 6px 10px"};
  font: inherit;
  cursor: pointer;
  outline: inherit;
  font-weight: 500;
  font-size: 14px;
  min-width: 100px;
  opacity: 1;
  transition: 200ms all ease;

  &:hover {
    opacity: 0.8;
  }

  ${(p) =>
    p.disabled &&
    css`
      pointer-events: none;
    `}
`;

const ButtonWrapper = styled.div`
  display: inline-block;
  border-radius: 20px;
  background: linear-gradient(white, white) padding-box,
    ${(p) => p.theme.buttonOrange} border-box;
  border: 1.5px solid transparent;
`;

const MediumButtonStyled = styled(ButtonStyled)`
  padding: 10px 15px;
`;

const defaultOptions = {
  loop: true,
  autoplay: true,
  animationData: loadButtonAnimation,
};

const ANIMATION_DIMENSIONS_SMALL = 36;
const ANIMATION_DIMENSIONS_MEDIUM = 41;

export const SmallButton = ({
  text,
  backgroundColor = "#ffffff",
  color = "#04021d",
  border,
  buttonStyle = "normal",
  disabled,
  onClick,
  isLoading,
}: IButton) => {
  if (buttonStyle === "empty") {
    return (
      <ButtonStyled
        onClick={onClick}
        backgroundColor="#ffffff"
        color={color}
        border={border}
        buttonStyle={buttonStyle}
        disabled={disabled}
        style={isLoading ? { paddingTop: 0, paddingBottom: 0 } : {}}
      >
        {isLoading ? (
          <Lottie
            options={defaultOptions}
            width={ANIMATION_DIMENSIONS_SMALL}
            height={ANIMATION_DIMENSIONS_SMALL}
          />
        ) : (
          text
        )}
      </ButtonStyled>
    );
  }
  if (buttonStyle === "emptyGradient") {
    return (
      <ButtonWrapper>
        <ButtonStyled
          onClick={onClick}
          backgroundColor={backgroundColor}
          color={color}
          border={border}
          buttonStyle={buttonStyle}
          disabled={disabled}
          style={{
            background: backgroundColor,
            WebkitBackgroundClip: "text",
            WebkitTextFillColor: "transparent",
            paddingTop: isLoading ? 0 : 7.5,
            paddingBottom: isLoading ? 0 : 7.5,
          }}
        >
          {isLoading ? (
            <Lottie
              options={defaultOptions}
              width={ANIMATION_DIMENSIONS_SMALL}
              height={ANIMATION_DIMENSIONS_SMALL}
            />
          ) : (
            text
          )}
        </ButtonStyled>
      </ButtonWrapper>
    );
  }
  return (
    <ButtonStyled
      backgroundColor={backgroundColor}
      color={color}
      onClick={onClick}
      border={border}
      buttonStyle={buttonStyle}
      disabled={disabled}
      style={isLoading ? { paddingTop: 0, paddingBottom: 0 } : {}}
    >
      {isLoading ? (
        <Lottie
          options={defaultOptions}
          width={ANIMATION_DIMENSIONS_SMALL}
          height={ANIMATION_DIMENSIONS_SMALL}
        />
      ) : (
        text
      )}
    </ButtonStyled>
  );
};

export const MediumButton = ({
  text,
  backgroundColor = "#ffffff",
  color = "#04021d",
  border,
  onClick,
  buttonStyle = "normal",
  disabled,
  isLoading,
}: IButton) => {
  if (buttonStyle === "empty") {
    return (
      <MediumButtonStyled
        onClick={onClick}
        backgroundColor="#ffffff"
        color={color}
        border={border}
        buttonStyle={buttonStyle}
        disabled={disabled}
        style={isLoading ? { paddingTop: 0, paddingBottom: 0 } : {}}
      >
        {isLoading ? (
          <Lottie
            options={defaultOptions}
            width={ANIMATION_DIMENSIONS_MEDIUM}
            height={ANIMATION_DIMENSIONS_MEDIUM}
          />
        ) : (
          text
        )}
      </MediumButtonStyled>
    );
  }
  if (buttonStyle === "emptyGradient") {
    return (
      <ButtonWrapper>
        <MediumButtonStyled
          onClick={onClick}
          backgroundColor={backgroundColor}
          color={color}
          border={border}
          buttonStyle={buttonStyle}
          disabled={disabled}
          style={{
            background: backgroundColor,
            WebkitBackgroundClip: "text",
            WebkitTextFillColor: "transparent",
            paddingTop: isLoading ? 0 : 7.5,
            paddingBottom: isLoading ? 0 : 7.5,
          }}
        >
          {isLoading ? (
            <Lottie
              options={defaultOptions}
              width={ANIMATION_DIMENSIONS_MEDIUM}
              height={ANIMATION_DIMENSIONS_MEDIUM}
            />
          ) : (
            text
          )}
        </MediumButtonStyled>
      </ButtonWrapper>
    );
  }
  return (
    <MediumButtonStyled
      onClick={onClick}
      backgroundColor={backgroundColor}
      color={color}
      border={border}
      buttonStyle={buttonStyle}
      disabled={disabled}
      style={isLoading ? { paddingTop: 0, paddingBottom: 0 } : {}}
    >
      {isLoading ? (
        <Lottie
          options={defaultOptions}
          width={ANIMATION_DIMENSIONS_MEDIUM}
          height={ANIMATION_DIMENSIONS_MEDIUM}
        />
      ) : (
        text
      )}
    </MediumButtonStyled>
  );
};
