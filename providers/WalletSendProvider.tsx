import { debounce } from "lodash";
import {
  createContext,
  Dispatch,
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import { SendModal } from "../components/wallet/sendreceive/SendModal";
import { useRegisterLightningPayment } from "../hooks/queries/tokens/useRegisterLightningPayment";
import { useTransferToAddress } from "../hooks/queries/tokens/useTransferToAddress";
import { useTransferTokens } from "../hooks/queries/tokens/useTransferTokens";
import useUserTokens from "../hooks/queries/tokens/useUserTokens";
import useUserSearch from "../hooks/queries/wallet/useUserSearch";
import { useGetLightningInvoiceInfo } from "../hooks/useGetLightningInvoiceInfo";
import { emptyTokenFrom, IUserToken } from "../models/Tokens";
import { ISearchUser, ISendRecipient } from "../models/Wallet";
import { formatRoundedDownTokenPrecision, satsToBitcoin } from "../utils";
import { SendWalletValueTypes } from "../utils/constants";
import { AuthContext } from "./AuthProvider";

export const WalletSendContext = createContext<{
  selectedToken: IUserToken;
  recipient: ISendRecipient | null;
  searchUsers: ISearchUser[];
  amount: number;
  isLoading: boolean;
  valueType: SendWalletValueTypes;
  fee: number;
  total: number;
  expected: number;
  error: string;

  handleSendOpen: (token: IUserToken) => void;
  setRecipient: Dispatch<any>;
  setSearchQuery: Dispatch<any>;
  setAmount: Dispatch<any>;
  setFee: Dispatch<any>;
  setValueType: (type: SendWalletValueTypes) => void;
  setModalOpen: Dispatch<any>;
  resetState: () => void;
  submitSend: () => Promise<void>;
}>({
  selectedToken: emptyTokenFrom,
  recipient: null,
  searchUsers: [],
  amount: 0,
  isLoading: false,
  valueType: SendWalletValueTypes.TOKEN,
  fee: 0,
  total: 0,
  expected: 0,
  error: "",

  handleSendOpen: (token: IUserToken) => {},
  setRecipient: () => {},
  setSearchQuery: () => {},
  setAmount: () => {},
  setFee: () => {},
  setModalOpen: Function,
  setValueType: () => {},
  resetState: () => {},
  submitSend: async () => Promise.resolve(),
});

export const WalletSendProvider: FunctionComponent = ({ children }) => {
  const [selectedToken, setSelectedToken] =
    useState<IUserToken>(emptyTokenFrom);
  const [recipient, setRecipient] = useState<ISendRecipient | null>(null);
  const [searchQuery, setSearchQuery] = useState<string>("");
  const [amount, setAmount] = useState<number>(0);
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [valueType, setValueType] = useState<SendWalletValueTypes>(
    SendWalletValueTypes.TOKEN
  );
  const [fee, setFee] = useState<number>(0);
  const [error, setError] = useState<string>("");

  const { currentUser } = useContext(AuthContext);
  const { data: searchUsers, isLoading } = useUserSearch({
    accountName: searchQuery,
    notifyOnChangeProps: ["data"],
  });
  const { data: userTokens } = useUserTokens({
    accountName: currentUser.actor,
    notifyOnChangeProps: ["data"],
  });

  const { mutateAsync: transferTokens } = useTransferTokens();
  const { mutateAsync: transferToAddress } = useTransferToAddress();
  const { mutateAsync: registerLightning } = useRegisterLightningPayment();
  const { getInfo } = useGetLightningInvoiceInfo();

  const tokenData = useMemo(() => {
    if (!userTokens) return;
    return userTokens.find((t) => t.symbol === selectedToken.symbol);
  }, [userTokens, selectedToken]);

  const handleSetSelectedToken = (token: IUserToken) => {
    setSearchQuery("");
    setAmount(0);
    setSelectedToken(token);
  };

  const resetState = useCallback(() => {
    setSelectedToken(emptyTokenFrom);
    setRecipient(null);
    setSearchQuery("");
    setAmount(0);
    setError("");
  }, []);

  const handleSendOpen = useCallback(
    (token: IUserToken) => {
      handleSetSelectedToken(token);
      setModalOpen(true);
    },
    [handleSetSelectedToken]
  );

  const handleErrorCheck = useCallback(
    debounce(({ amount, recipient, tokenData, total, expected }) => {
      const validateAmount = () => {
        if (expected < 0) {
          setError("Insufficent Amount");
          return false;
        }
        if (recipient?.type === "bitcoin" && amount < 0.002) {
          setError("Minimum 200k Sats");
          return false;
        }
        if (recipient?.type === "lightning" && amount > 0.002) {
          setError("Maximum 200k Sats");
          return false;
        }
        if (recipient?.type === "lightning" && amount < 0.00000006) {
          setError("Minimum 6 Sats");
          return false;
        }
        return true;
      };

      setError("");

      if (!recipient && !amount) return;
      if (!validateAmount()) return;

      if (Number(amount) == 0) {
        return setError("Insufficient Amount");
      }

      if (
        Number(total) &&
        Number(total) > 0 &&
        Number(total) > tokenData.unstaked
      ) {
        return setError("Insufficient Balance");
      }
    }, 200),
    []
  );

  const total = useMemo(() => {
    if (!amount || amount == 0 || !fee) return "--";
    if (Number(amount) === Number(tokenData.unstaked)) {
      return `${formatRoundedDownTokenPrecision({
        value: Number(amount),
        precision: tokenData.precision,
      })}`;
    }
    if (recipient?.type === "bitcoin") {
      return `${formatRoundedDownTokenPrecision({
        value: Number(amount) + Number(fee),
        precision: tokenData.precision,
      })}`;
    }
    if (recipient?.type === "lightning") {
      return `${formatRoundedDownTokenPrecision({
        value: Number(amount) + Number(satsToBitcoin(fee)),
        precision: tokenData.precision,
      })}`;
    }
    if (recipient?.type === "ethereum") {
      return `${formatRoundedDownTokenPrecision({
        value: Number(amount) + fee,
        precision: tokenData.precision,
      })}`;
    }
    return `${formatRoundedDownTokenPrecision({
      value: Number(amount),
      precision: tokenData.precision,
    })}`;
  }, [amount, fee, recipient, tokenData]);

  const expected = useMemo(() => {
    if (!amount) return 0;
    if (Number(amount) === Number(tokenData.unstaked)) {
      if (recipient?.type === "bitcoin") {
        return `${formatRoundedDownTokenPrecision({
          value: amount - fee,
          precision: tokenData.precision,
        })}`;
      }
      if (recipient?.type === "lightning") {
        return `${formatRoundedDownTokenPrecision({
          value: amount - satsToBitcoin(fee),
          precision: tokenData.precision,
        })}`;
      }
      if (recipient?.type === "ethereum") {
        return `${formatRoundedDownTokenPrecision({
          value: Number(amount) - fee,
          precision: tokenData.precision,
        })}`;
      }
    }
    return `${formatRoundedDownTokenPrecision({
      value: Number(amount),
      precision: tokenData.precision,
    })}`;
  }, [amount, fee, recipient, tokenData]);

  const submitSend = useCallback(async () => {
    if (!recipient || error) return;
    if (recipient.type === "libre") {
      await transferTokens({
        tokenName: selectedToken.symbol,
        to: String(recipient?.destination),
        from: currentUser.actor,
        quantity: `${formatRoundedDownTokenPrecision({
          value: Number(total),
          precision: selectedToken.precision,
        })} ${selectedToken.symbol}`,
        memo: "",
      });
    }
    if (recipient.type === "bitcoin" || recipient.type === "ethereum") {
      const contractName =
        process.env.NEXT_PUBLIC_ENV === "testnet"
          ? "eosio.token"
          : selectedToken.symbol === "PBTC"
          ? "btc.ptokens"
          : selectedToken.symbol === "PUSDT"
          ? "usdt.ptokens"
          : "eosio.token";
      await transferToAddress({
        memo: String(recipient?.destination),
        from: currentUser.actor,
        quantity: `${formatRoundedDownTokenPrecision({
          value: Number(total),
          precision: selectedToken.precision,
        })} ${selectedToken.symbol}`,
        tokenContract: contractName,
        chainId: recipient.type === "bitcoin" ? "01EC97DE" : "005fe7f9",
      });
    }
    if (recipient.type === "lightning") {
      const registerResult = await registerLightning({
        invoice: recipient.destination,
        accountName: currentUser.actor,
      });
      await transferTokens({
        tokenName: selectedToken.symbol,
        to: "bank.libre",
        from: currentUser.actor,
        quantity: `${formatRoundedDownTokenPrecision({
          value: registerResult.amount
            ? Number(registerResult.amount) + Number(satsToBitcoin(fee))
            : Number(total),
          precision: selectedToken.precision,
        })} ${selectedToken.symbol}`,
        memo: registerResult.id,
      });
    }
  }, [recipient, currentUser, amount, total, selectedToken, error]);

  const handleLightningData = async () => {
    if (recipient?.type === "lightning") {
      const info = await getInfo(recipient.destination.invoice);
      if (!info || !info.amountSatoshis) return;
      setAmount(satsToBitcoin(info?.amountSatoshis));
    }
  };

  useEffect(() => {
    handleErrorCheck({ amount, recipient, tokenData, total, expected });
  }, [recipient, amount, tokenData, total, expected]);

  useEffect(() => {
    handleLightningData();
  }, [recipient]);

  const value = {
    selectedToken,
    recipient,
    searchUsers,
    amount,
    isLoading,
    valueType,
    fee,
    total,
    expected,
    error,

    handleSendOpen,
    setRecipient,
    setAmount,
    setFee,
    setSearchQuery,
    setValueType,
    setModalOpen,
    resetState,
    submitSend,
  };

  return (
    <WalletSendContext.Provider value={value}>
      {children}
      <SendModal open={modalOpen} handleClose={() => setModalOpen(false)} />
    </WalletSendContext.Provider>
  );
};
