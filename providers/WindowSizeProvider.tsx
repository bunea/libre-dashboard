import { createContext, FunctionComponent } from "react";

export const WindowSizeContext = createContext<{
  windowWidth: number;
  isSmallMobile: boolean;
  isMobile: boolean;
  isTablet: boolean;
  isLaptop: boolean;
  isDesktop: boolean;
}>({
  windowWidth: 1200,
  isSmallMobile: false,
  isMobile: false,
  isTablet: false,
  isLaptop: false,
  isDesktop: false,
});

export const WindowSizeProvider: FunctionComponent = ({ children }) => {
  const isSSR = typeof window === "undefined";
  const windowWidth = isSSR ? 1200 : window.innerWidth;
  const isSmallMobile = isSSR ? false : window.innerWidth <= 400;
  const isMobile = isSSR ? false : window.innerWidth <= 600;
  const isTablet = isSSR ? false : window.innerWidth <= 970;
  const isLaptop = isSSR ? false : window.innerWidth <= 1224;
  const isDesktop = isSSR ? false : window.innerWidth > 1224;

  const value = {
    windowWidth,
    isSmallMobile,
    isMobile,
    isTablet,
    isLaptop,
    isDesktop,
  };

  return (
    <WindowSizeContext.Provider value={value}>
      {children}
    </WindowSizeContext.Provider>
  );
};
